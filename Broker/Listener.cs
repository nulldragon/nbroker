﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Packets;

namespace Broker
{
    public class Listener
    {
        public static int Port = 15000;
        private readonly UdpClient udp = new UdpClient(Port);
        public void StartListening()
        {
            Console.WriteLine("Listening on " + Port);
            this.udp.BeginReceive(Receive, new object());
        }

        private void Receive(IAsyncResult ar)
        {
            //IPEndPoint ip = new IPEndPoint(IPAddress.Any, Port);
            IPEndPoint ip = new IPEndPoint(IPAddress.Any, 0);//linux any port
            byte[] bytes = udp.EndReceive(ar, ref ip);

            var packType = Packets.PacketType.GetPacketType(ref bytes);

            if(packType == typeof(Packets.Heartbeat))
            {
                ProcessHeartBeat(bytes, ip);
            }
            else if(packType == typeof(Packets.Request))
            {
                ProcessRequest(bytes, ip);
            }
            else
            {
                Console.Write("Unknown packet tyep: "+ bytes[0].ToString());
            }

            string message = Encoding.ASCII.GetString(bytes);
            StartListening();
        }

        private void ProcessRequest(byte[] bytes, IPEndPoint ip)
        {
            try
            {
                var rqt = Packets.Request.fromBytes(bytes);

                Console.WriteLine("Id: " + rqt.deviceId + "\tVersion: " + rqt.version + "\tTopic " + rqt.topic + "\tFrom " + ip.Address.ToString());
               
                SendRequestAck(rqt, ip);

            }
            catch(Exception ex)
            {
#if DEBUG
                Console.Write(ex.Message + Environment.NewLine + ex.StackTrace);
#endif
                Console.WriteLine("Bad Request packet");
            }
        }
      
        private void ProcessHeartBeat(byte[] bytes, IPEndPoint ip)
        {
            try
            {
                var beat = Packets.Heartbeat.fromBytes(bytes);

                Console.WriteLine("Id: " + beat.deviceId + "\tVersion: " + beat.version + "\tFrom " + ip.Address.ToString());

                NodeManager.BeatNode(beat, ip.Address);
                SendAck(ip);
            }
            catch(Exception ex)
            {
#if DEBUG
                Console.Write(ex.Message + Environment.NewLine + ex.StackTrace);
#endif
                Console.WriteLine("Bad heartbeat packet");
            }
        }

        private void SendAck(IPEndPoint ip)
        {
            UdpClient client = new UdpClient();

            Packets.HeartACK ack = new Packets.HeartACK();
            ack.serverId = 0;//irrelevant
            ack.version = 1;

            byte[] bytes = Packets.HeartACK.getBytes(ack);

            client.Send(bytes, bytes.Length, ip);
            client.Close();

            Console.WriteLine("ACK");
        }

        private void SendRequestAck(Packets.Request request, IPEndPoint ip)
        {
            UdpClient client = new UdpClient();
            byte status = 0;

            UInt16 key = NodeManager.RequestNode(request, ip.Address, ref status);//get notifications when they come in from push packets

            Packets.RequestResponse ack = new Packets.RequestResponse();
            ack.serverId = 0;//irrelevant
            ack.version = 1;
            ack.requestKeyId = key;
            ack.status = status;

            byte[] bytes = Packets.RequestResponse.getBytes(ack);

            client.Send(bytes, bytes.Length, ip);
            client.Close();

            Console.WriteLine("ReqACK (" + key + ")\tStatus: " + status );
        }

    }
}
