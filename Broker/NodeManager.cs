﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    public class NodeManager
    {
        /// <summary>
        /// We give devices a 60 second lax period by default, (heart beat default is 15seconds by default)
        /// If device falls outside this period we consider it disconnected
        /// </summary>
        public static int LaxPeriod = 60;

        public static List<Node> Nodes = new List<Node>();

        //handles connects etc
        public static void BeatNode(Packets.Heartbeat beat, System.Net.IPAddress addr)
        {
            Node node = GetNode(beat.deviceId, addr);

            node.LastSeen = DateTime.Now;
            node.IsConnected = true;
            node.BeatInterval = beat.beatRate;
        }

        private static Node GetNode(UInt16 deviceId, IPAddress addr)
        {
            var node = (from n in Nodes
                        where n.DeviceId == deviceId
                        && n.IP.ToString() == addr.ToString()
                        select n).FirstOrDefault();

            if (node == null)
            {
                //add node
                node = new Node();
                node.DeviceId = deviceId;
                node.IP = addr;
                Nodes.Add(node);
            }

            return node;
        }

        public static void SetupTimer()
        {
            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 1000;
            t.AutoReset = true;
            t.Elapsed += (s, e) =>
            {
                //keep track of nodes and timeout
                foreach(var n in Nodes)
                {
                    //process
                    //send requests ect
                }
            };
            t.Start();
        }

        public static ushort RequestNode(Packets.Request rqt, IPAddress addr, ref byte status)
        {
            Node node = GetNode(rqt.deviceId, addr);
            if (node == null)
            {
                status = 0;
                return 0;//Node goes oopsy
            }

            if (node.Requests.Where(a => a.Topic == rqt.topic).FirstOrDefault() == null)
            {
                //good to request
                NodeRequests nr = new NodeRequests();
                nr.Topic = rqt.topic;
                node.Requests.Add(nr);

                status = 1;//Node now registered for Topic
                return nr.Key;
            }
            else
            {
                status = 2;//Node alreay has this Topic requested
                return node.Requests.Where(a => a.Topic == rqt.topic).First().Key;
            }
        }
    }

    public class Node
    {
        public System.Net.IPAddress IP
        {
            get; set;
        }
        public UInt16 DeviceId
        {
            get; set;
        }

        public byte BeatInterval
        {
            get; set;
        }
        public DateTime LastSeen
        {
            get; set;
        }

        public bool IsConnected
        {
            get; set;
        }

        public List<NodeRequests> Requests{ get; set;}

        public Node()
        {
            IP = null;
            DeviceId = 0;
            BeatInterval = 0;
            LastSeen = DateTime.MinValue;
            IsConnected = false;
            Requests = new List<NodeRequests>();
        }
    }

    public class NodeRequests
    {
        //key structure like MQTT
        //todo wild card handling

        public string Topic { get; set; }
        public UInt16 Key { get; private set; }

        //Give unique id
        private static UInt16 KeyCount = 0;

        public NodeRequests()
        {
            Topic = string.Empty;
            Key = KeyCount;
            KeyCount++;
        }
    }
}
