﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Broker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Broker v1");

            Listener listen = new Listener();
            listen.StartListening();

            ConsoleKeyInfo key = Console.ReadKey();

            while(key.Key != ConsoleKey.Q)
            {                
                key = Console.ReadKey();
            }
        }
    }
}
