﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nControl
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            lblAppName.Text = Application.ProductName;
            lblVersion.Text = Application.ProductVersion;
            lblCredits.Text = "Written and Developed by S.Antonio 2015\n";
            lblInfo.Text = "This software is provided as is. Good luck. God Speed.";
            lblTodo.Text = "Update nRelay Firmware to filter based upon device id\nUpdate nRelay Firmware to allow changing of update rates";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
