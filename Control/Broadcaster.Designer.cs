﻿namespace HomeControl
{
    partial class Broadcaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBroadcast = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnQuickMessage = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkPortA1 = new System.Windows.Forms.CheckBox();
            this.chkPortA2 = new System.Windows.Forms.CheckBox();
            this.chkPortA3 = new System.Windows.Forms.CheckBox();
            this.chkPortA4 = new System.Windows.Forms.CheckBox();
            this.chkPortA5 = new System.Windows.Forms.CheckBox();
            this.chkPortA6 = new System.Windows.Forms.CheckBox();
            this.chkPortA7 = new System.Windows.Forms.CheckBox();
            this.chkPortA0 = new System.Windows.Forms.CheckBox();
            this.chkPortB0 = new System.Windows.Forms.CheckBox();
            this.chkPortB7 = new System.Windows.Forms.CheckBox();
            this.chkPortB6 = new System.Windows.Forms.CheckBox();
            this.chkPortB5 = new System.Windows.Forms.CheckBox();
            this.chkPortB4 = new System.Windows.Forms.CheckBox();
            this.chkPortB3 = new System.Windows.Forms.CheckBox();
            this.chkPortB2 = new System.Windows.Forms.CheckBox();
            this.chkPortB1 = new System.Windows.Forms.CheckBox();
            this.chkPortC0 = new System.Windows.Forms.CheckBox();
            this.chkPortC7 = new System.Windows.Forms.CheckBox();
            this.chkPortC6 = new System.Windows.Forms.CheckBox();
            this.chkPortC5 = new System.Windows.Forms.CheckBox();
            this.chkPortC4 = new System.Windows.Forms.CheckBox();
            this.chkPortC3 = new System.Windows.Forms.CheckBox();
            this.chkPortC2 = new System.Windows.Forms.CheckBox();
            this.chkPortC1 = new System.Windows.Forms.CheckBox();
            this.chkPortC8 = new System.Windows.Forms.CheckBox();
            this.chkPortC15 = new System.Windows.Forms.CheckBox();
            this.chkPortC14 = new System.Windows.Forms.CheckBox();
            this.chkPortC13 = new System.Windows.Forms.CheckBox();
            this.chkPortC12 = new System.Windows.Forms.CheckBox();
            this.chkPortC11 = new System.Windows.Forms.CheckBox();
            this.chkPortC10 = new System.Windows.Forms.CheckBox();
            this.chkPortC9 = new System.Windows.Forms.CheckBox();
            this.chkPortB8 = new System.Windows.Forms.CheckBox();
            this.chkPortB15 = new System.Windows.Forms.CheckBox();
            this.chkPortB14 = new System.Windows.Forms.CheckBox();
            this.chkPortB13 = new System.Windows.Forms.CheckBox();
            this.chkPortB12 = new System.Windows.Forms.CheckBox();
            this.chkPortB11 = new System.Windows.Forms.CheckBox();
            this.chkPortB10 = new System.Windows.Forms.CheckBox();
            this.chkPortB9 = new System.Windows.Forms.CheckBox();
            this.chkPortA8 = new System.Windows.Forms.CheckBox();
            this.chkPortA15 = new System.Windows.Forms.CheckBox();
            this.chkPortA14 = new System.Windows.Forms.CheckBox();
            this.chkPortA13 = new System.Windows.Forms.CheckBox();
            this.chkPortA12 = new System.Windows.Forms.CheckBox();
            this.chkPortA11 = new System.Windows.Forms.CheckBox();
            this.chkPortA10 = new System.Windows.Forms.CheckBox();
            this.chkPortA9 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnBroadcast
            // 
            this.btnBroadcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBroadcast.Location = new System.Drawing.Point(822, 12);
            this.btnBroadcast.Name = "btnBroadcast";
            this.btnBroadcast.Size = new System.Drawing.Size(75, 23);
            this.btnBroadcast.TabIndex = 2;
            this.btnBroadcast.Text = "Send";
            this.btnBroadcast.UseVisualStyleBackColor = true;
            this.btnBroadcast.Click += new System.EventHandler(this.btnBroadcast_Click);
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(710, 433);
            this.listBox1.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(12, 14);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(804, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // btnQuickMessage
            // 
            this.btnQuickMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuickMessage.Location = new System.Drawing.Point(822, 81);
            this.btnQuickMessage.Name = "btnQuickMessage";
            this.btnQuickMessage.Size = new System.Drawing.Size(75, 23);
            this.btnQuickMessage.TabIndex = 6;
            this.btnQuickMessage.Text = "Send";
            this.btnQuickMessage.UseVisualStyleBackColor = true;
            this.btnQuickMessage.Click += new System.EventHandler(this.btnQuickMessage_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(736, 85);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(59, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "On/Off";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14"});
            this.comboBox1.Location = new System.Drawing.Point(822, 54);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(75, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.Text = "1";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(733, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pin";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkPortA1
            // 
            this.chkPortA1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA1.AutoSize = true;
            this.chkPortA1.Enabled = false;
            this.chkPortA1.Location = new System.Drawing.Point(57, 479);
            this.chkPortA1.Name = "chkPortA1";
            this.chkPortA1.Size = new System.Drawing.Size(39, 17);
            this.chkPortA1.TabIndex = 7;
            this.chkPortA1.Tag = "10";
            this.chkPortA1.Text = "A1";
            this.chkPortA1.UseVisualStyleBackColor = true;
            // 
            // chkPortA2
            // 
            this.chkPortA2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA2.AutoSize = true;
            this.chkPortA2.Enabled = false;
            this.chkPortA2.Location = new System.Drawing.Point(102, 479);
            this.chkPortA2.Name = "chkPortA2";
            this.chkPortA2.Size = new System.Drawing.Size(39, 17);
            this.chkPortA2.TabIndex = 8;
            this.chkPortA2.Tag = "9";
            this.chkPortA2.Text = "A2";
            this.chkPortA2.UseVisualStyleBackColor = true;
            // 
            // chkPortA3
            // 
            this.chkPortA3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA3.AutoSize = true;
            this.chkPortA3.Enabled = false;
            this.chkPortA3.Location = new System.Drawing.Point(147, 479);
            this.chkPortA3.Name = "chkPortA3";
            this.chkPortA3.Size = new System.Drawing.Size(39, 17);
            this.chkPortA3.TabIndex = 9;
            this.chkPortA3.Tag = "8";
            this.chkPortA3.Text = "A3";
            this.chkPortA3.UseVisualStyleBackColor = true;
            // 
            // chkPortA4
            // 
            this.chkPortA4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA4.AutoSize = true;
            this.chkPortA4.Enabled = false;
            this.chkPortA4.Location = new System.Drawing.Point(192, 479);
            this.chkPortA4.Name = "chkPortA4";
            this.chkPortA4.Size = new System.Drawing.Size(39, 17);
            this.chkPortA4.TabIndex = 10;
            this.chkPortA4.Tag = "7";
            this.chkPortA4.Text = "A4";
            this.chkPortA4.UseVisualStyleBackColor = true;
            // 
            // chkPortA5
            // 
            this.chkPortA5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA5.AutoSize = true;
            this.chkPortA5.Enabled = false;
            this.chkPortA5.Location = new System.Drawing.Point(237, 479);
            this.chkPortA5.Name = "chkPortA5";
            this.chkPortA5.Size = new System.Drawing.Size(39, 17);
            this.chkPortA5.TabIndex = 11;
            this.chkPortA5.Tag = "6";
            this.chkPortA5.Text = "A5";
            this.chkPortA5.UseVisualStyleBackColor = true;
            // 
            // chkPortA6
            // 
            this.chkPortA6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA6.AutoSize = true;
            this.chkPortA6.Enabled = false;
            this.chkPortA6.Location = new System.Drawing.Point(282, 479);
            this.chkPortA6.Name = "chkPortA6";
            this.chkPortA6.Size = new System.Drawing.Size(39, 17);
            this.chkPortA6.TabIndex = 12;
            this.chkPortA6.Tag = "5";
            this.chkPortA6.Text = "A6";
            this.chkPortA6.UseVisualStyleBackColor = true;
            // 
            // chkPortA7
            // 
            this.chkPortA7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA7.AutoSize = true;
            this.chkPortA7.Enabled = false;
            this.chkPortA7.Location = new System.Drawing.Point(327, 479);
            this.chkPortA7.Name = "chkPortA7";
            this.chkPortA7.Size = new System.Drawing.Size(39, 17);
            this.chkPortA7.TabIndex = 13;
            this.chkPortA7.Tag = "4";
            this.chkPortA7.Text = "A7";
            this.chkPortA7.UseVisualStyleBackColor = true;
            // 
            // chkPortA0
            // 
            this.chkPortA0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA0.AutoSize = true;
            this.chkPortA0.Enabled = false;
            this.chkPortA0.Location = new System.Drawing.Point(12, 479);
            this.chkPortA0.Name = "chkPortA0";
            this.chkPortA0.Size = new System.Drawing.Size(39, 17);
            this.chkPortA0.TabIndex = 14;
            this.chkPortA0.Tag = "11";
            this.chkPortA0.Text = "A0";
            this.chkPortA0.UseVisualStyleBackColor = true;
            // 
            // chkPortB0
            // 
            this.chkPortB0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB0.AutoSize = true;
            this.chkPortB0.Enabled = false;
            this.chkPortB0.Location = new System.Drawing.Point(12, 502);
            this.chkPortB0.Name = "chkPortB0";
            this.chkPortB0.Size = new System.Drawing.Size(39, 17);
            this.chkPortB0.TabIndex = 22;
            this.chkPortB0.Tag = "3";
            this.chkPortB0.Text = "B0";
            this.chkPortB0.UseVisualStyleBackColor = true;
            // 
            // chkPortB7
            // 
            this.chkPortB7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB7.AutoSize = true;
            this.chkPortB7.Enabled = false;
            this.chkPortB7.Location = new System.Drawing.Point(327, 502);
            this.chkPortB7.Name = "chkPortB7";
            this.chkPortB7.Size = new System.Drawing.Size(39, 17);
            this.chkPortB7.TabIndex = 21;
            this.chkPortB7.Tag = "15";
            this.chkPortB7.Text = "B7";
            this.chkPortB7.UseVisualStyleBackColor = true;
            // 
            // chkPortB6
            // 
            this.chkPortB6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB6.AutoSize = true;
            this.chkPortB6.Enabled = false;
            this.chkPortB6.Location = new System.Drawing.Point(282, 502);
            this.chkPortB6.Name = "chkPortB6";
            this.chkPortB6.Size = new System.Drawing.Size(39, 17);
            this.chkPortB6.TabIndex = 20;
            this.chkPortB6.Tag = "16";
            this.chkPortB6.Text = "B6";
            this.chkPortB6.UseVisualStyleBackColor = true;
            // 
            // chkPortB5
            // 
            this.chkPortB5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB5.AutoSize = true;
            this.chkPortB5.Enabled = false;
            this.chkPortB5.Location = new System.Drawing.Point(237, 502);
            this.chkPortB5.Name = "chkPortB5";
            this.chkPortB5.Size = new System.Drawing.Size(39, 17);
            this.chkPortB5.TabIndex = 19;
            this.chkPortB5.Tag = "17";
            this.chkPortB5.Text = "B5";
            this.chkPortB5.UseVisualStyleBackColor = true;
            // 
            // chkPortB4
            // 
            this.chkPortB4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB4.AutoSize = true;
            this.chkPortB4.Enabled = false;
            this.chkPortB4.Location = new System.Drawing.Point(192, 502);
            this.chkPortB4.Name = "chkPortB4";
            this.chkPortB4.Size = new System.Drawing.Size(39, 17);
            this.chkPortB4.TabIndex = 18;
            this.chkPortB4.Tag = "18";
            this.chkPortB4.Text = "B4";
            this.chkPortB4.UseVisualStyleBackColor = true;
            // 
            // chkPortB3
            // 
            this.chkPortB3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB3.AutoSize = true;
            this.chkPortB3.Enabled = false;
            this.chkPortB3.Location = new System.Drawing.Point(147, 502);
            this.chkPortB3.Name = "chkPortB3";
            this.chkPortB3.Size = new System.Drawing.Size(39, 17);
            this.chkPortB3.TabIndex = 17;
            this.chkPortB3.Tag = "19";
            this.chkPortB3.Text = "B3";
            this.chkPortB3.UseVisualStyleBackColor = true;
            // 
            // chkPortB2
            // 
            this.chkPortB2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB2.AutoSize = true;
            this.chkPortB2.Enabled = false;
            this.chkPortB2.Location = new System.Drawing.Point(102, 502);
            this.chkPortB2.Name = "chkPortB2";
            this.chkPortB2.Size = new System.Drawing.Size(39, 17);
            this.chkPortB2.TabIndex = 16;
            this.chkPortB2.Tag = "2";
            this.chkPortB2.Text = "B2";
            this.chkPortB2.UseVisualStyleBackColor = true;
            // 
            // chkPortB1
            // 
            this.chkPortB1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB1.AutoSize = true;
            this.chkPortB1.Enabled = false;
            this.chkPortB1.Location = new System.Drawing.Point(57, 502);
            this.chkPortB1.Name = "chkPortB1";
            this.chkPortB1.Size = new System.Drawing.Size(39, 17);
            this.chkPortB1.TabIndex = 15;
            this.chkPortB1.Tag = "33";
            this.chkPortB1.Text = "B1";
            this.chkPortB1.UseVisualStyleBackColor = true;
            // 
            // chkPortC0
            // 
            this.chkPortC0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC0.AutoSize = true;
            this.chkPortC0.Enabled = false;
            this.chkPortC0.Location = new System.Drawing.Point(12, 525);
            this.chkPortC0.Name = "chkPortC0";
            this.chkPortC0.Size = new System.Drawing.Size(39, 17);
            this.chkPortC0.TabIndex = 30;
            this.chkPortC0.Text = "C0";
            this.chkPortC0.UseVisualStyleBackColor = true;
            // 
            // chkPortC7
            // 
            this.chkPortC7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC7.AutoSize = true;
            this.chkPortC7.Enabled = false;
            this.chkPortC7.Location = new System.Drawing.Point(327, 525);
            this.chkPortC7.Name = "chkPortC7";
            this.chkPortC7.Size = new System.Drawing.Size(39, 17);
            this.chkPortC7.TabIndex = 29;
            this.chkPortC7.Text = "C7";
            this.chkPortC7.UseVisualStyleBackColor = true;
            // 
            // chkPortC6
            // 
            this.chkPortC6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC6.AutoSize = true;
            this.chkPortC6.Enabled = false;
            this.chkPortC6.Location = new System.Drawing.Point(282, 525);
            this.chkPortC6.Name = "chkPortC6";
            this.chkPortC6.Size = new System.Drawing.Size(39, 17);
            this.chkPortC6.TabIndex = 28;
            this.chkPortC6.Text = "C6";
            this.chkPortC6.UseVisualStyleBackColor = true;
            // 
            // chkPortC5
            // 
            this.chkPortC5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC5.AutoSize = true;
            this.chkPortC5.Enabled = false;
            this.chkPortC5.Location = new System.Drawing.Point(237, 525);
            this.chkPortC5.Name = "chkPortC5";
            this.chkPortC5.Size = new System.Drawing.Size(39, 17);
            this.chkPortC5.TabIndex = 27;
            this.chkPortC5.Text = "C5";
            this.chkPortC5.UseVisualStyleBackColor = true;
            // 
            // chkPortC4
            // 
            this.chkPortC4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC4.AutoSize = true;
            this.chkPortC4.Enabled = false;
            this.chkPortC4.Location = new System.Drawing.Point(192, 525);
            this.chkPortC4.Name = "chkPortC4";
            this.chkPortC4.Size = new System.Drawing.Size(39, 17);
            this.chkPortC4.TabIndex = 26;
            this.chkPortC4.Text = "C4";
            this.chkPortC4.UseVisualStyleBackColor = true;
            // 
            // chkPortC3
            // 
            this.chkPortC3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC3.AutoSize = true;
            this.chkPortC3.Enabled = false;
            this.chkPortC3.Location = new System.Drawing.Point(147, 525);
            this.chkPortC3.Name = "chkPortC3";
            this.chkPortC3.Size = new System.Drawing.Size(39, 17);
            this.chkPortC3.TabIndex = 25;
            this.chkPortC3.Text = "C3";
            this.chkPortC3.UseVisualStyleBackColor = true;
            // 
            // chkPortC2
            // 
            this.chkPortC2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC2.AutoSize = true;
            this.chkPortC2.Enabled = false;
            this.chkPortC2.Location = new System.Drawing.Point(102, 525);
            this.chkPortC2.Name = "chkPortC2";
            this.chkPortC2.Size = new System.Drawing.Size(39, 17);
            this.chkPortC2.TabIndex = 24;
            this.chkPortC2.Text = "C2";
            this.chkPortC2.UseVisualStyleBackColor = true;
            // 
            // chkPortC1
            // 
            this.chkPortC1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC1.AutoSize = true;
            this.chkPortC1.Enabled = false;
            this.chkPortC1.Location = new System.Drawing.Point(57, 525);
            this.chkPortC1.Name = "chkPortC1";
            this.chkPortC1.Size = new System.Drawing.Size(39, 17);
            this.chkPortC1.TabIndex = 23;
            this.chkPortC1.Text = "C1";
            this.chkPortC1.UseVisualStyleBackColor = true;
            // 
            // chkPortC8
            // 
            this.chkPortC8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC8.AutoSize = true;
            this.chkPortC8.Enabled = false;
            this.chkPortC8.Location = new System.Drawing.Point(372, 525);
            this.chkPortC8.Name = "chkPortC8";
            this.chkPortC8.Size = new System.Drawing.Size(39, 17);
            this.chkPortC8.TabIndex = 54;
            this.chkPortC8.Text = "C8";
            this.chkPortC8.UseVisualStyleBackColor = true;
            // 
            // chkPortC15
            // 
            this.chkPortC15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC15.AutoSize = true;
            this.chkPortC15.Enabled = false;
            this.chkPortC15.Location = new System.Drawing.Point(687, 524);
            this.chkPortC15.Name = "chkPortC15";
            this.chkPortC15.Size = new System.Drawing.Size(45, 17);
            this.chkPortC15.TabIndex = 53;
            this.chkPortC15.Tag = "12";
            this.chkPortC15.Text = "C15";
            this.chkPortC15.UseVisualStyleBackColor = true;
            // 
            // chkPortC14
            // 
            this.chkPortC14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC14.AutoSize = true;
            this.chkPortC14.Enabled = false;
            this.chkPortC14.Location = new System.Drawing.Point(642, 525);
            this.chkPortC14.Name = "chkPortC14";
            this.chkPortC14.Size = new System.Drawing.Size(45, 17);
            this.chkPortC14.TabIndex = 52;
            this.chkPortC14.Tag = "13";
            this.chkPortC14.Text = "C14";
            this.chkPortC14.UseVisualStyleBackColor = true;
            // 
            // chkPortC13
            // 
            this.chkPortC13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC13.AutoSize = true;
            this.chkPortC13.Enabled = false;
            this.chkPortC13.Location = new System.Drawing.Point(597, 525);
            this.chkPortC13.Name = "chkPortC13";
            this.chkPortC13.Size = new System.Drawing.Size(45, 17);
            this.chkPortC13.TabIndex = 51;
            this.chkPortC13.Tag = "14";
            this.chkPortC13.Text = "C13";
            this.chkPortC13.UseVisualStyleBackColor = true;
            // 
            // chkPortC12
            // 
            this.chkPortC12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC12.AutoSize = true;
            this.chkPortC12.Enabled = false;
            this.chkPortC12.Location = new System.Drawing.Point(552, 525);
            this.chkPortC12.Name = "chkPortC12";
            this.chkPortC12.Size = new System.Drawing.Size(45, 17);
            this.chkPortC12.TabIndex = 50;
            this.chkPortC12.Text = "C12";
            this.chkPortC12.UseVisualStyleBackColor = true;
            // 
            // chkPortC11
            // 
            this.chkPortC11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC11.AutoSize = true;
            this.chkPortC11.Enabled = false;
            this.chkPortC11.Location = new System.Drawing.Point(507, 525);
            this.chkPortC11.Name = "chkPortC11";
            this.chkPortC11.Size = new System.Drawing.Size(45, 17);
            this.chkPortC11.TabIndex = 49;
            this.chkPortC11.Text = "C11";
            this.chkPortC11.UseVisualStyleBackColor = true;
            // 
            // chkPortC10
            // 
            this.chkPortC10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC10.AutoSize = true;
            this.chkPortC10.Enabled = false;
            this.chkPortC10.Location = new System.Drawing.Point(462, 525);
            this.chkPortC10.Name = "chkPortC10";
            this.chkPortC10.Size = new System.Drawing.Size(45, 17);
            this.chkPortC10.TabIndex = 48;
            this.chkPortC10.Text = "C10";
            this.chkPortC10.UseVisualStyleBackColor = true;
            // 
            // chkPortC9
            // 
            this.chkPortC9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortC9.AutoSize = true;
            this.chkPortC9.Enabled = false;
            this.chkPortC9.Location = new System.Drawing.Point(417, 525);
            this.chkPortC9.Name = "chkPortC9";
            this.chkPortC9.Size = new System.Drawing.Size(39, 17);
            this.chkPortC9.TabIndex = 47;
            this.chkPortC9.Text = "C9";
            this.chkPortC9.UseVisualStyleBackColor = true;
            // 
            // chkPortB8
            // 
            this.chkPortB8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB8.AutoSize = true;
            this.chkPortB8.Enabled = false;
            this.chkPortB8.Location = new System.Drawing.Point(372, 502);
            this.chkPortB8.Name = "chkPortB8";
            this.chkPortB8.Size = new System.Drawing.Size(39, 17);
            this.chkPortB8.TabIndex = 46;
            this.chkPortB8.Tag = "32";
            this.chkPortB8.Text = "B8";
            this.chkPortB8.UseVisualStyleBackColor = true;
            // 
            // chkPortB15
            // 
            this.chkPortB15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB15.AutoSize = true;
            this.chkPortB15.Enabled = false;
            this.chkPortB15.Location = new System.Drawing.Point(687, 502);
            this.chkPortB15.Name = "chkPortB15";
            this.chkPortB15.Size = new System.Drawing.Size(45, 17);
            this.chkPortB15.TabIndex = 45;
            this.chkPortB15.Tag = "28";
            this.chkPortB15.Text = "B15";
            this.chkPortB15.UseVisualStyleBackColor = true;
            // 
            // chkPortB14
            // 
            this.chkPortB14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB14.AutoSize = true;
            this.chkPortB14.Enabled = false;
            this.chkPortB14.Location = new System.Drawing.Point(642, 502);
            this.chkPortB14.Name = "chkPortB14";
            this.chkPortB14.Size = new System.Drawing.Size(45, 17);
            this.chkPortB14.TabIndex = 44;
            this.chkPortB14.Tag = "29";
            this.chkPortB14.Text = "B14";
            this.chkPortB14.UseVisualStyleBackColor = true;
            // 
            // chkPortB13
            // 
            this.chkPortB13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB13.AutoSize = true;
            this.chkPortB13.Enabled = false;
            this.chkPortB13.Location = new System.Drawing.Point(597, 502);
            this.chkPortB13.Name = "chkPortB13";
            this.chkPortB13.Size = new System.Drawing.Size(45, 17);
            this.chkPortB13.TabIndex = 43;
            this.chkPortB13.Tag = "30";
            this.chkPortB13.Text = "B13";
            this.chkPortB13.UseVisualStyleBackColor = true;
            // 
            // chkPortB12
            // 
            this.chkPortB12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB12.AutoSize = true;
            this.chkPortB12.Enabled = false;
            this.chkPortB12.Location = new System.Drawing.Point(552, 502);
            this.chkPortB12.Name = "chkPortB12";
            this.chkPortB12.Size = new System.Drawing.Size(45, 17);
            this.chkPortB12.TabIndex = 42;
            this.chkPortB12.Tag = "31";
            this.chkPortB12.Text = "B12";
            this.chkPortB12.UseVisualStyleBackColor = true;
            // 
            // chkPortB11
            // 
            this.chkPortB11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB11.AutoSize = true;
            this.chkPortB11.Enabled = false;
            this.chkPortB11.Location = new System.Drawing.Point(507, 502);
            this.chkPortB11.Name = "chkPortB11";
            this.chkPortB11.Size = new System.Drawing.Size(45, 17);
            this.chkPortB11.TabIndex = 41;
            this.chkPortB11.Tag = "0";
            this.chkPortB11.Text = "B11";
            this.chkPortB11.UseVisualStyleBackColor = true;
            // 
            // chkPortB10
            // 
            this.chkPortB10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB10.AutoSize = true;
            this.chkPortB10.Enabled = false;
            this.chkPortB10.Location = new System.Drawing.Point(462, 502);
            this.chkPortB10.Name = "chkPortB10";
            this.chkPortB10.Size = new System.Drawing.Size(45, 17);
            this.chkPortB10.TabIndex = 40;
            this.chkPortB10.Tag = "1";
            this.chkPortB10.Text = "B10";
            this.chkPortB10.UseVisualStyleBackColor = true;
            // 
            // chkPortB9
            // 
            this.chkPortB9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortB9.AutoSize = true;
            this.chkPortB9.Enabled = false;
            this.chkPortB9.Location = new System.Drawing.Point(417, 502);
            this.chkPortB9.Name = "chkPortB9";
            this.chkPortB9.Size = new System.Drawing.Size(39, 17);
            this.chkPortB9.TabIndex = 39;
            this.chkPortB9.Text = "B9";
            this.chkPortB9.UseVisualStyleBackColor = true;
            // 
            // chkPortA8
            // 
            this.chkPortA8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA8.AutoSize = true;
            this.chkPortA8.Enabled = false;
            this.chkPortA8.Location = new System.Drawing.Point(372, 479);
            this.chkPortA8.Name = "chkPortA8";
            this.chkPortA8.Size = new System.Drawing.Size(39, 17);
            this.chkPortA8.TabIndex = 38;
            this.chkPortA8.Tag = "27";
            this.chkPortA8.Text = "A8";
            this.chkPortA8.UseVisualStyleBackColor = true;
            // 
            // chkPortA15
            // 
            this.chkPortA15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA15.AutoSize = true;
            this.chkPortA15.Enabled = false;
            this.chkPortA15.Location = new System.Drawing.Point(687, 479);
            this.chkPortA15.Name = "chkPortA15";
            this.chkPortA15.Size = new System.Drawing.Size(45, 17);
            this.chkPortA15.TabIndex = 37;
            this.chkPortA15.Tag = "20";
            this.chkPortA15.Text = "A15";
            this.chkPortA15.UseVisualStyleBackColor = true;
            // 
            // chkPortA14
            // 
            this.chkPortA14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA14.AutoSize = true;
            this.chkPortA14.Enabled = false;
            this.chkPortA14.Location = new System.Drawing.Point(642, 479);
            this.chkPortA14.Name = "chkPortA14";
            this.chkPortA14.Size = new System.Drawing.Size(45, 17);
            this.chkPortA14.TabIndex = 36;
            this.chkPortA14.Tag = "21";
            this.chkPortA14.Text = "A14";
            this.chkPortA14.UseVisualStyleBackColor = true;
            // 
            // chkPortA13
            // 
            this.chkPortA13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA13.AutoSize = true;
            this.chkPortA13.Enabled = false;
            this.chkPortA13.Location = new System.Drawing.Point(597, 479);
            this.chkPortA13.Name = "chkPortA13";
            this.chkPortA13.Size = new System.Drawing.Size(45, 17);
            this.chkPortA13.TabIndex = 35;
            this.chkPortA13.Tag = "22";
            this.chkPortA13.Text = "A13";
            this.chkPortA13.UseVisualStyleBackColor = true;
            // 
            // chkPortA12
            // 
            this.chkPortA12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA12.AutoSize = true;
            this.chkPortA12.Enabled = false;
            this.chkPortA12.Location = new System.Drawing.Point(552, 479);
            this.chkPortA12.Name = "chkPortA12";
            this.chkPortA12.Size = new System.Drawing.Size(45, 17);
            this.chkPortA12.TabIndex = 34;
            this.chkPortA12.Tag = "23";
            this.chkPortA12.Text = "A12";
            this.chkPortA12.UseVisualStyleBackColor = true;
            // 
            // chkPortA11
            // 
            this.chkPortA11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA11.AutoSize = true;
            this.chkPortA11.Enabled = false;
            this.chkPortA11.Location = new System.Drawing.Point(507, 479);
            this.chkPortA11.Name = "chkPortA11";
            this.chkPortA11.Size = new System.Drawing.Size(45, 17);
            this.chkPortA11.TabIndex = 33;
            this.chkPortA11.Tag = "24";
            this.chkPortA11.Text = "A11";
            this.chkPortA11.UseVisualStyleBackColor = true;
            // 
            // chkPortA10
            // 
            this.chkPortA10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA10.AutoSize = true;
            this.chkPortA10.Enabled = false;
            this.chkPortA10.Location = new System.Drawing.Point(462, 479);
            this.chkPortA10.Name = "chkPortA10";
            this.chkPortA10.Size = new System.Drawing.Size(45, 17);
            this.chkPortA10.TabIndex = 32;
            this.chkPortA10.Tag = "25";
            this.chkPortA10.Text = "A10";
            this.chkPortA10.UseVisualStyleBackColor = true;
            // 
            // chkPortA9
            // 
            this.chkPortA9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkPortA9.AutoSize = true;
            this.chkPortA9.Enabled = false;
            this.chkPortA9.Location = new System.Drawing.Point(417, 479);
            this.chkPortA9.Name = "chkPortA9";
            this.chkPortA9.Size = new System.Drawing.Size(39, 17);
            this.chkPortA9.TabIndex = 31;
            this.chkPortA9.Tag = "26";
            this.chkPortA9.Text = "A9";
            this.chkPortA9.UseVisualStyleBackColor = true;
            // 
            // Broadcaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 553);
            this.Controls.Add(this.chkPortC8);
            this.Controls.Add(this.chkPortC15);
            this.Controls.Add(this.chkPortC14);
            this.Controls.Add(this.chkPortC13);
            this.Controls.Add(this.chkPortC12);
            this.Controls.Add(this.chkPortC11);
            this.Controls.Add(this.chkPortC10);
            this.Controls.Add(this.chkPortC9);
            this.Controls.Add(this.chkPortB8);
            this.Controls.Add(this.chkPortB15);
            this.Controls.Add(this.chkPortB14);
            this.Controls.Add(this.chkPortB13);
            this.Controls.Add(this.chkPortB12);
            this.Controls.Add(this.chkPortB11);
            this.Controls.Add(this.chkPortB10);
            this.Controls.Add(this.chkPortB9);
            this.Controls.Add(this.chkPortA8);
            this.Controls.Add(this.chkPortA15);
            this.Controls.Add(this.chkPortA14);
            this.Controls.Add(this.chkPortA13);
            this.Controls.Add(this.chkPortA12);
            this.Controls.Add(this.chkPortA11);
            this.Controls.Add(this.chkPortA10);
            this.Controls.Add(this.chkPortA9);
            this.Controls.Add(this.chkPortC0);
            this.Controls.Add(this.chkPortC7);
            this.Controls.Add(this.chkPortC6);
            this.Controls.Add(this.chkPortC5);
            this.Controls.Add(this.chkPortC4);
            this.Controls.Add(this.chkPortC3);
            this.Controls.Add(this.chkPortC2);
            this.Controls.Add(this.chkPortC1);
            this.Controls.Add(this.chkPortB0);
            this.Controls.Add(this.chkPortB7);
            this.Controls.Add(this.chkPortB6);
            this.Controls.Add(this.chkPortB5);
            this.Controls.Add(this.chkPortB4);
            this.Controls.Add(this.chkPortB3);
            this.Controls.Add(this.chkPortB2);
            this.Controls.Add(this.chkPortB1);
            this.Controls.Add(this.chkPortA0);
            this.Controls.Add(this.chkPortA7);
            this.Controls.Add(this.chkPortA6);
            this.Controls.Add(this.chkPortA5);
            this.Controls.Add(this.chkPortA4);
            this.Controls.Add(this.chkPortA3);
            this.Controls.Add(this.chkPortA2);
            this.Controls.Add(this.chkPortA1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnQuickMessage);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnBroadcast);
            this.Name = "Broadcaster";
            this.Text = "Broadcaster";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBroadcast;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnQuickMessage;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkPortA1;
        private System.Windows.Forms.CheckBox chkPortA2;
        private System.Windows.Forms.CheckBox chkPortA3;
        private System.Windows.Forms.CheckBox chkPortA4;
        private System.Windows.Forms.CheckBox chkPortA5;
        private System.Windows.Forms.CheckBox chkPortA6;
        private System.Windows.Forms.CheckBox chkPortA7;
        private System.Windows.Forms.CheckBox chkPortA0;
        private System.Windows.Forms.CheckBox chkPortB0;
        private System.Windows.Forms.CheckBox chkPortB7;
        private System.Windows.Forms.CheckBox chkPortB6;
        private System.Windows.Forms.CheckBox chkPortB5;
        private System.Windows.Forms.CheckBox chkPortB4;
        private System.Windows.Forms.CheckBox chkPortB3;
        private System.Windows.Forms.CheckBox chkPortB2;
        private System.Windows.Forms.CheckBox chkPortB1;
        private System.Windows.Forms.CheckBox chkPortC0;
        private System.Windows.Forms.CheckBox chkPortC7;
        private System.Windows.Forms.CheckBox chkPortC6;
        private System.Windows.Forms.CheckBox chkPortC5;
        private System.Windows.Forms.CheckBox chkPortC4;
        private System.Windows.Forms.CheckBox chkPortC3;
        private System.Windows.Forms.CheckBox chkPortC2;
        private System.Windows.Forms.CheckBox chkPortC1;
        private System.Windows.Forms.CheckBox chkPortC8;
        private System.Windows.Forms.CheckBox chkPortC15;
        private System.Windows.Forms.CheckBox chkPortC14;
        private System.Windows.Forms.CheckBox chkPortC13;
        private System.Windows.Forms.CheckBox chkPortC12;
        private System.Windows.Forms.CheckBox chkPortC11;
        private System.Windows.Forms.CheckBox chkPortC10;
        private System.Windows.Forms.CheckBox chkPortC9;
        private System.Windows.Forms.CheckBox chkPortB8;
        private System.Windows.Forms.CheckBox chkPortB15;
        private System.Windows.Forms.CheckBox chkPortB14;
        private System.Windows.Forms.CheckBox chkPortB13;
        private System.Windows.Forms.CheckBox chkPortB12;
        private System.Windows.Forms.CheckBox chkPortB11;
        private System.Windows.Forms.CheckBox chkPortB10;
        private System.Windows.Forms.CheckBox chkPortB9;
        private System.Windows.Forms.CheckBox chkPortA8;
        private System.Windows.Forms.CheckBox chkPortA15;
        private System.Windows.Forms.CheckBox chkPortA14;
        private System.Windows.Forms.CheckBox chkPortA13;
        private System.Windows.Forms.CheckBox chkPortA12;
        private System.Windows.Forms.CheckBox chkPortA11;
        private System.Windows.Forms.CheckBox chkPortA10;
        private System.Windows.Forms.CheckBox chkPortA9;
    }
}