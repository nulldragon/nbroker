﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using nControl.Models;

namespace HomeControl
{
    public partial class Broadcaster : Form
    {
        public Broadcaster()
        {
            InitializeComponent();

            listBox1.Items.Clear();
            listBox1.Items.Insert(0, "Broadcaster v" + Application.ProductVersion);

            SimpleUDPServer.Start();
            listBox1.Items.Insert(0, "Now listening on port " + SimpleUDPServer.Port);

            this.textBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;

            //SuggestStrings will have the logic to return array of strings either from cache/db
            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();
            collection.AddRange(SuggestStrings());

            this.textBox1.AutoCompleteCustomSource = collection;

            Timer t = new Timer();
            t.Interval = 500;
            t.Tick += (o, s) =>
            {
                while (SimpleUDPServer.Messages.Count > 0)
                {
                    var msg = SimpleUDPServer.Messages.Pop();
                    listBox1.Items.Insert(0, DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " Received\t" + msg.Message + " from " + msg.Source);

                    if (msg.Message.Contains("/STATE/"))
                        ParseState(msg);
                }
                
            };
            t.Start();

            AssignEvents();
        }
        
        private void AssignEvents()
        {
            foreach(var c in this.Controls)
            {
                if(c is CheckBox)
                {
                    if (((CheckBox)c).Tag != null)
                    {
                        ((CheckBox)c).CheckedChanged += ChkPort_CheckedChanged;
                        ((CheckBox)c).Enabled = true;
                    }
                }
            }
        }

        private void ChkPort_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;
            if (chk == null)
                return;
            SendPinUpdate(chk.Checked, Convert.ToInt32(chk.Tag));
        }

        private void SendPinUpdate(bool state, int pinNumber)
        {
            string device = nControl.Properties.Settings.Default.DefaultDevice;
            if(state)
                SendMessage(device + "/PIN_ON/" + pinNumber);
            else
                SendMessage(device + "/PIN_OFF/" + pinNumber);
        }

        private void btnBroadcast_Click(object sender, EventArgs e)
        {
            SendMessage(textBox1.Text.Trim());
            textBox1.Text = string.Empty;
        }

        private void SendMessage(string v)
        {
            listBox1.Items.Insert(0, "Sending\t" + v);

            (new System.Threading.Thread(() =>
            {
                SimpleUDPServer.SendCommand(v);
            })
            { IsBackground = true }).Start();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                SendMessage(textBox1.Text.Trim());
                textBox1.Text = string.Empty;
            }
        }

        private string[] SuggestStrings()
        {
            return new string[] {
                "217BBD37-E4BF-4D8A-A43B-3887F68350EA",
                "217BBD37-E4BF-4D8A-A43B-3887F68350EA/PIN_ON/",
                "217BBD37-E4BF-4D8A-A43B-3887F68350EA/PIN_OFF/",
                "/STATE/",
                "/IDENTIFY/",
            };
        }

        private void btnQuickMessage_Click(object sender, EventArgs e)
        {
            int offset = 0;// 17; //was 17 but now device takes care of offset
            int pin = 0;
            if(int.TryParse(comboBox1.Text, out pin))
            {
                int targetPin = pin + offset;

                string device = nControl.Properties.Settings.Default.DefaultDevice;

                if (checkBox1.Checked)
                    SendMessage(device + "/PIN_ON/" + targetPin);
                else
                    SendMessage(device + "/PIN_OFF/" + targetPin);
            }
        }

        private void ParseState(nMessage msg)
        {
            try
            {
                string str = msg.Message.Substring(msg.Message.LastIndexOf('/') + 1);
                var rawA = str.Split(',')[0];
                var rawB = str.Split(',')[1];
                var rawC = str.Split(',')[2];

                int A = int.Parse(rawA);
                int B = int.Parse(rawB);
                int C = int.Parse(rawC);

                chkPortA0.Checked = CheckBit(A, 0);
                chkPortA1.Checked = CheckBit(A, 1);
                chkPortA2.Checked = CheckBit(A, 2);
                chkPortA3.Checked = CheckBit(A, 3);
                chkPortA4.Checked = CheckBit(A, 4);
                chkPortA5.Checked = CheckBit(A, 5);
                chkPortA6.Checked = CheckBit(A, 6);
                chkPortA7.Checked = CheckBit(A, 7);

                chkPortB0.Checked = CheckBit(B, 0);
                chkPortB1.Checked = CheckBit(B, 1);
                chkPortB2.Checked = CheckBit(B, 2);
                chkPortB3.Checked = CheckBit(B, 3);
                chkPortB4.Checked = CheckBit(B, 4);
                chkPortB5.Checked = CheckBit(B, 5);
                chkPortB6.Checked = CheckBit(B, 6);
                chkPortB7.Checked = CheckBit(B, 7);

                chkPortC0.Checked = CheckBit(C, 0);
                chkPortC1.Checked = CheckBit(C, 1);
                chkPortC2.Checked = CheckBit(C, 2);
                chkPortC3.Checked = CheckBit(C, 3);
                chkPortC4.Checked = CheckBit(C, 4);
                chkPortC5.Checked = CheckBit(C, 5);
                chkPortC6.Checked = CheckBit(C, 6);
                chkPortC7.Checked = CheckBit(C, 7);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
            }
        }

        private bool CheckBit(int b, byte bitNumber)
        {
            var bit = (b & (1 << bitNumber - 1)) != 0;
            return bit;
        }
    }
}

