﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeControl
{
    public partial class MasterWindow : Form
    {
        private RunFlags runflags;
        private NotifyIcon trayIcon;

        private Scheduler sch;

        public MasterWindow()
        {
            InitializeComponent();
            this.runflags = RunFlags.None;
        }

        public MasterWindow(RunFlags runFlags)
        {
            InitializeComponent();
            this.runflags = runFlags;

            if ((runflags & RunFlags.Background) == RunFlags.Background)
                RunInBackground();
        }

        #region Events
        private void btnRelays_Click(object sender, EventArgs e)
        {
            RelayStationDeviceStatusDisplay form = new RelayStationDeviceStatusDisplay();
            form.Show();
        }

        private void btnBroadcast_Click(object sender, EventArgs e)
        {
            Broadcaster form = new Broadcaster();
            form.Show();
        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {
            Scheduler form = new Scheduler();
            form.Show();
        }
        #endregion

        private void RunInBackground()
        {
            var menuItems = new MenuItem[]
            {
                new MenuItem("S&cheduler",(s,e) => { this.btnSchedule_Click(s,e); }),
                new MenuItem("&View Station",(s,e) => { this.btnRelays_Click(s,e); }),
                new MenuItem("&Broadcast",(s,e) => { this.btnBroadcast_Click(s,e); }),

                new MenuItem("-"),

                new MenuItem("&About",(s,e) => { var abt = new nControl.About(); abt.Show(); }),

                new MenuItem("-"),

                new MenuItem("E&xit",(s,e) => { this.Close(); }),
            };
            var trayMenu = new ContextMenu(menuItems);

            trayIcon = new NotifyIcon();
            trayIcon.Text = Application.ProductName;
            trayIcon.Icon = nControl.Properties.Resources.calendar_white_161;

             // Add menu to tray icon and show it.
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;

            //hide
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new System.Drawing.Point(-2000, -2000);
            this.Size = new System.Drawing.Size(1, 1);
            this.Visible = false; // Hide form window.
            this.Hide();

            RunScheduler();
        }

        private void RunScheduler()
        {
            //start the scheduler running and hide it
            sch = new Scheduler(true);
            sch.Visible = false;
            sch.ShowInTaskbar = false;
            sch.Hide();
        }
    }
}
