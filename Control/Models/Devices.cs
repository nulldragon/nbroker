﻿using nControl.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeControl.Models
{
    public enum DeviceType
    {
        RelayStation,
        Other,
        Unknown
    }

    public class Device
    {
        public string GUID { get; set; }//master

        [XmlIgnore]
        public IPAddress IP { get; set; }

        [XmlElement("MasterIP")]
        public string MasterIPForXml
        {
            get
            {
                return IP == null ? string.Empty : IP.ToString();
            }
            set
            {
                IP = string.IsNullOrEmpty(value) ? null :
                IPAddress.Parse(value);
            }
        }

        public DateTime Created { get; set; }
        public DateTime LastSeen { get; set; }

        public DeviceType DeviceType { get; set; }

        public bool IsOnline()
        {
            Ping x = new Ping();
            PingReply reply = x.Send(this.IP);

            if (reply.Status == IPStatus.Success)
                return true;

            return false;
        }


        #region Static Methods

        private static string DefaultLocation = nControl.Properties.Settings.Default.WorkingDirectory + "Devices.xml";

        public static List<Device> Devices
        {
            get
            {
                //Load the default directory for devices
                if (_devices == null)
                    _devices = Load();
                if (_devices == null)//still null no file?
                    _devices = new List<Device>();

                return _devices;
            }
            set
            {
                _devices = value;
            }
        }
        private static List<Device> _devices;
        
        public static List<Device> Load()
        {
            if (File.Exists(DefaultLocation))
            {
                FileInfo fi = new FileInfo(DefaultLocation);
                if (fi.Length == 0)
                    return null;

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Device>));

                using (var f = File.OpenRead(DefaultLocation))
                {
                    var rp = xmlSerializer.Deserialize(f);
                    return rp as List<Device>;
                }
            }
            else
            {
                Console.WriteLine(DefaultLocation + " could not be found");
                System.IO.File.Create(DefaultLocation);
            }

            return null;
        }

        public static List<Device> Load(string fn)
        {
            if (File.Exists(fn))
            {
                FileInfo fi = new FileInfo(fn);
                if (fi.Length == 0)
                    return null;

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Device>));

                using (var f = File.OpenRead(fn))
                {
                    var rp = xmlSerializer.Deserialize(f);
                    return rp as List<Device>;
                }
            }
            else
            {
                Console.WriteLine(fn + " could not be found");
            }

            return null;
        }

        public static void Add(nMessage msg)
        {
            string guid = string.Empty;
            if (msg.Message.Contains("/"))
                guid = msg.Message.Split('/')[0];
            else
                guid = msg.Message;

            Devices.Add(new Device()
            {
                GUID = guid,
                DeviceType = DeviceType.RelayStation,
                Created = DateTime.Now,
                IP = msg.Source,
                LastSeen = DateTime.Now,
            });

            Save();
        }

        private static void Save()
        {
            //Save to default location
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Device>));

            using (var f = File.Open(DefaultLocation, FileMode.Create))
            {
                xmlSerializer.Serialize(f, Devices);
            }
        }

        public static bool Exists(nMessage msg)
        {
            string guid = string.Empty;
            if (msg.Message.Contains("/"))
                guid = msg.Message.Split('/')[0];
            else
                guid = msg.Message;

            if (String.IsNullOrEmpty(guid))
                return false;

            foreach(var d in Devices)
            {
                if (d.GUID == guid)
                    return true;
            }

            return false;
        }

        public static void Update(nMessage msg)
        {
            string guid = string.Empty;
            if (msg.Message.Contains("/"))
                guid = msg.Message.Split('/')[0];
            else
                guid = msg.Message;

            if (String.IsNullOrEmpty(guid))
                return;

            var dev = Devices.Where(a => a.GUID == guid).First();

            dev.IP = msg.Source;
            dev.LastSeen = DateTime.Now;

            Save();
        }

        #endregion
    }
}
