﻿using HomeControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nControl.Models
{
    public class RelayIOPin
    {
        public int PinNumber;
        public string PinName;
        public List<Schedule> schedule;

        public bool IsOn;

        public DateTime LastUpdate;
    }
}
