﻿using nControl.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeControl.Models
{
    public class Relay
    {
        public static List<RelayIOPin> Load(string fn)
        {
            if (File.Exists(fn))
            {
                FileInfo fi = new FileInfo(fn);
                if (fi.Length == 0)
                    return null;

                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<RelayIOPin>));

                using (var f = File.OpenRead(fn))
                {
                    var rp = xmlSerializer.Deserialize(f);

                    return rp as List<RelayIOPin>;
                }
            }
            else
            {
                Console.WriteLine(fn + " could not be found");
            }

            return null;
        }
    }
}
