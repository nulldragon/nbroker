﻿using HomeControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nControl.Models
{
    public enum ScheduleType
    {
        DayOfWeek,
        Even,
        Odd
    }

    public class Schedule
    {
        //public DateTime? date;

        public DayOfWeek dayOfWeek;
        public UInt16 hour;
        public UInt16 minute;

        public uint durationMinutes;
        //public bool IsRecuring;
        public ScheduleType scheduleType;
    }
}
