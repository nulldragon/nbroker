﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace nControl.Models
{
    public class StationSchedule : INotifyPropertyChanged
    {
        public DayOfWeek day { get { return _day; } set { SetField(ref _day, value); } }
        private DayOfWeek _day;

        public DateTime startTime { get { return _startTime; } set { SetField(ref _startTime, value); } }
        private DateTime _startTime;

        private TimeSpan m_TimeSinceLastEvent;

        [XmlIgnore]
        public TimeSpan Duration
        {
            get { return m_TimeSinceLastEvent; }
            set { m_TimeSinceLastEvent = value; NotifyField(); }
        }

        // Pretend property for serialization
        [XmlElement("TimeSinceLastEvent")]
        public long TimeSinceLastEventTicks
        {
            get { return m_TimeSinceLastEvent.Ticks; }
            set { m_TimeSinceLastEvent = new TimeSpan(value); NotifyField("Duration"); }
        }

        //Todo replace this with RelayIOPin
        public string DeviceId { get { return _deviceId; } set { SetField(ref _deviceId, value); } }
        private string _deviceId;
        public int PinNumber { get { return _pinNumber; } set { SetField(ref _pinNumber, value); } }
        private int _pinNumber;

        public bool IsMaster { get { return _isMaster; } set { SetField(ref _isMaster, value); } }
        private bool _isMaster;

        public StationSchedule()
        {
            day = DayOfWeek.Monday;
            startTime = new DateTime(2000, 1, 1, 12, 0, 0);
            Duration = new TimeSpan(0, 15, 0);
            DeviceId = "00000000-0000-0000-0000-000000000000";
            PinNumber = 0;
        }

        /// <summary>
        /// Checks the schedule for this device and the current system time to determine if the pin is scheduled to be ON
        /// </summary>
        /// <returns>True if pin is On, False if pin is OFF</returns>
        public bool IsOn()
        {

            if (this.day == DateTime.Now.DayOfWeek)
            {
                var dStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, startTime.Hour, startTime.Minute, 0);
                var dEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, startTime.Hour, startTime.Minute, 0);
                dEnd = dEnd.AddMinutes(Duration.TotalMinutes);

                if (dStart >= DateTime.Now && DateTime.Now <= dEnd)
                {
                    return true;
                }
            }
            return false;

        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected bool NotifyField([System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
        {
            OnPropertyChanged(propertyName);
            return true;
        }
        #endregion
    }
}
