﻿using nControl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeControl.Models
{
    public class WeeklySchedule
    {
        private string fileName;

        public List<StationSchedule> scheduleItems { get; set; }

        public WeeklySchedule()
        {
            scheduleItems = new List<StationSchedule>();

            fileName = nControl.Properties.Settings.Default.WorkingDirectory + "schedule.xml";
        }

        public bool SaveToDisk()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(scheduleItems.GetType());

            using (var f = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlSerializer.Serialize(f, scheduleItems);
            }

            return true;
        }

        public bool LoadFromDisk()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof (List<StationSchedule>));

            using (var f = File.OpenRead(fileName))
            {
                var rp = xmlSerializer.Deserialize(f);
                scheduleItems =  rp as List<StationSchedule>;
                if (scheduleItems != null)
                {
                    //validate here
                    return Validate(scheduleItems);
                }
            }

            return false;
        }

        private bool Validate(List<StationSchedule> scheduleItems)
        {
            //check for overlap
            //check durations etc
            return true;
        }
    }
    
    
}
