﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace nControl.Models
{
    public class nMessage
    {
        public string Message { get; set; }
        public IPAddress Source { get; set; }

        public override string ToString()
        {
            return Message + " from " + Source;
        }
    }
}
