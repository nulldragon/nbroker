﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeControl
{
    public enum RunFlags
    {
        None = 0,
        Background = 1,
        Debug = 2,

    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //SEt default directories
            if(nControl.Utilities.OS.IsLinux)
            {
                nControl.Properties.Settings.Default.WorkingDirectory = "~/.ncontrol/";
            }
            else
            {
                nControl.Properties.Settings.Default.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\ncontrol\\";
            }
            nControl.Properties.Settings.Default.Save();

            if (!System.IO.Directory.Exists(nControl.Properties.Settings.Default.WorkingDirectory))
                System.IO.Directory.CreateDirectory(nControl.Properties.Settings.Default.WorkingDirectory);

            var runFlags = RunFlags.None;

            //process cmd arguments
            if (args.Contains("-b") || args.Contains("--background"))
                runFlags |= RunFlags.Background;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MasterWindow(runFlags));
        }
    }
}
