﻿namespace HomeControl
{
    partial class RelayStationDeviceStatusDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPin1 = new System.Windows.Forms.Button();
            this.btnPin2 = new System.Windows.Forms.Button();
            this.btnPin3 = new System.Windows.Forms.Button();
            this.btnPin4 = new System.Windows.Forms.Button();
            this.btnPin5 = new System.Windows.Forms.Button();
            this.btnPin6 = new System.Windows.Forms.Button();
            this.btnPin7 = new System.Windows.Forms.Button();
            this.btnPin8 = new System.Windows.Forms.Button();
            this.btnPin9 = new System.Windows.Forms.Button();
            this.btnPin10 = new System.Windows.Forms.Button();
            this.btnPin11 = new System.Windows.Forms.Button();
            this.btnPin12 = new System.Windows.Forms.Button();
            this.btnPin13 = new System.Windows.Forms.Button();
            this.btnPin14 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.cmbDevices = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDevice = new System.Windows.Forms.Label();
            this.btnWorkingDirectory = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsWorkingDirectory = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPin1
            // 
            this.btnPin1.Location = new System.Drawing.Point(3, 183);
            this.btnPin1.Name = "btnPin1";
            this.btnPin1.Size = new System.Drawing.Size(75, 23);
            this.btnPin1.TabIndex = 0;
            this.btnPin1.Text = "Station 1";
            this.btnPin1.UseVisualStyleBackColor = true;
            // 
            // btnPin2
            // 
            this.btnPin2.Location = new System.Drawing.Point(3, 154);
            this.btnPin2.Name = "btnPin2";
            this.btnPin2.Size = new System.Drawing.Size(75, 23);
            this.btnPin2.TabIndex = 1;
            this.btnPin2.Text = "Station 2";
            this.btnPin2.UseVisualStyleBackColor = true;
            // 
            // btnPin3
            // 
            this.btnPin3.Location = new System.Drawing.Point(3, 125);
            this.btnPin3.Name = "btnPin3";
            this.btnPin3.Size = new System.Drawing.Size(75, 23);
            this.btnPin3.TabIndex = 2;
            this.btnPin3.Text = "Station 3";
            this.btnPin3.UseVisualStyleBackColor = true;
            // 
            // btnPin4
            // 
            this.btnPin4.Location = new System.Drawing.Point(3, 96);
            this.btnPin4.Name = "btnPin4";
            this.btnPin4.Size = new System.Drawing.Size(75, 23);
            this.btnPin4.TabIndex = 3;
            this.btnPin4.Text = "Station 4";
            this.btnPin4.UseVisualStyleBackColor = true;
            // 
            // btnPin5
            // 
            this.btnPin5.Location = new System.Drawing.Point(3, 67);
            this.btnPin5.Name = "btnPin5";
            this.btnPin5.Size = new System.Drawing.Size(75, 23);
            this.btnPin5.TabIndex = 4;
            this.btnPin5.Text = "Station 5";
            this.btnPin5.UseVisualStyleBackColor = true;
            // 
            // btnPin6
            // 
            this.btnPin6.Location = new System.Drawing.Point(3, 38);
            this.btnPin6.Name = "btnPin6";
            this.btnPin6.Size = new System.Drawing.Size(75, 23);
            this.btnPin6.TabIndex = 5;
            this.btnPin6.Text = "Station 6";
            this.btnPin6.UseVisualStyleBackColor = true;
            // 
            // btnPin7
            // 
            this.btnPin7.Location = new System.Drawing.Point(3, 9);
            this.btnPin7.Name = "btnPin7";
            this.btnPin7.Size = new System.Drawing.Size(75, 23);
            this.btnPin7.TabIndex = 6;
            this.btnPin7.Text = "Station 7";
            this.btnPin7.UseVisualStyleBackColor = true;
            // 
            // btnPin8
            // 
            this.btnPin8.Location = new System.Drawing.Point(3, 183);
            this.btnPin8.Name = "btnPin8";
            this.btnPin8.Size = new System.Drawing.Size(75, 23);
            this.btnPin8.TabIndex = 7;
            this.btnPin8.Text = "Station 8";
            this.btnPin8.UseVisualStyleBackColor = true;
            // 
            // btnPin9
            // 
            this.btnPin9.Location = new System.Drawing.Point(3, 154);
            this.btnPin9.Name = "btnPin9";
            this.btnPin9.Size = new System.Drawing.Size(75, 23);
            this.btnPin9.TabIndex = 8;
            this.btnPin9.Text = "Station 9";
            this.btnPin9.UseVisualStyleBackColor = true;
            // 
            // btnPin10
            // 
            this.btnPin10.Location = new System.Drawing.Point(3, 125);
            this.btnPin10.Name = "btnPin10";
            this.btnPin10.Size = new System.Drawing.Size(75, 23);
            this.btnPin10.TabIndex = 9;
            this.btnPin10.Text = "Station 10";
            this.btnPin10.UseVisualStyleBackColor = true;
            // 
            // btnPin11
            // 
            this.btnPin11.Location = new System.Drawing.Point(3, 96);
            this.btnPin11.Name = "btnPin11";
            this.btnPin11.Size = new System.Drawing.Size(75, 23);
            this.btnPin11.TabIndex = 10;
            this.btnPin11.Text = "Station 11";
            this.btnPin11.UseVisualStyleBackColor = true;
            // 
            // btnPin12
            // 
            this.btnPin12.Location = new System.Drawing.Point(3, 67);
            this.btnPin12.Name = "btnPin12";
            this.btnPin12.Size = new System.Drawing.Size(75, 23);
            this.btnPin12.TabIndex = 12;
            this.btnPin12.Text = "Station 12";
            this.btnPin12.UseVisualStyleBackColor = true;
            // 
            // btnPin13
            // 
            this.btnPin13.Location = new System.Drawing.Point(3, 38);
            this.btnPin13.Name = "btnPin13";
            this.btnPin13.Size = new System.Drawing.Size(75, 23);
            this.btnPin13.TabIndex = 13;
            this.btnPin13.Text = "Station 13";
            this.btnPin13.UseVisualStyleBackColor = true;
            // 
            // btnPin14
            // 
            this.btnPin14.Location = new System.Drawing.Point(3, 9);
            this.btnPin14.Name = "btnPin14";
            this.btnPin14.Size = new System.Drawing.Size(75, 23);
            this.btnPin14.TabIndex = 14;
            this.btnPin14.Text = "Station 14";
            this.btnPin14.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnPin1);
            this.flowLayoutPanel1.Controls.Add(this.btnPin2);
            this.flowLayoutPanel1.Controls.Add(this.btnPin3);
            this.flowLayoutPanel1.Controls.Add(this.btnPin4);
            this.flowLayoutPanel1.Controls.Add(this.btnPin5);
            this.flowLayoutPanel1.Controls.Add(this.btnPin6);
            this.flowLayoutPanel1.Controls.Add(this.btnPin7);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(180, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(85, 209);
            this.flowLayoutPanel1.TabIndex = 15;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnPin8);
            this.flowLayoutPanel2.Controls.Add(this.btnPin9);
            this.flowLayoutPanel2.Controls.Add(this.btnPin10);
            this.flowLayoutPanel2.Controls.Add(this.btnPin11);
            this.flowLayoutPanel2.Controls.Add(this.btnPin12);
            this.flowLayoutPanel2.Controls.Add(this.btnPin13);
            this.flowLayoutPanel2.Controls.Add(this.btnPin14);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(539, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(85, 209);
            this.flowLayoutPanel2.TabIndex = 16;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 269);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(725, 95);
            this.listBox1.TabIndex = 17;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 370);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(725, 20);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = "Command";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(743, 368);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 19;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // cmbDevices
            // 
            this.cmbDevices.FormattingEnabled = true;
            this.cmbDevices.Location = new System.Drawing.Point(59, 12);
            this.cmbDevices.Name = "cmbDevices";
            this.cmbDevices.Size = new System.Drawing.Size(577, 21);
            this.cmbDevices.TabIndex = 20;
            this.cmbDevices.SelectedValueChanged += new System.EventHandler(this.cmbDevices_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Device";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDevice, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 48);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(806, 215);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // txtDevice
            // 
            this.txtDevice.AutoSize = true;
            this.txtDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtDevice.Location = new System.Drawing.Point(271, 0);
            this.txtDevice.Name = "txtDevice";
            this.txtDevice.Size = new System.Drawing.Size(262, 13);
            this.txtDevice.TabIndex = 17;
            this.txtDevice.Text = "Device Id";
            this.txtDevice.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnWorkingDirectory
            // 
            this.btnWorkingDirectory.Location = new System.Drawing.Point(642, 10);
            this.btnWorkingDirectory.Name = "btnWorkingDirectory";
            this.btnWorkingDirectory.Size = new System.Drawing.Size(176, 23);
            this.btnWorkingDirectory.TabIndex = 23;
            this.btnWorkingDirectory.Text = "Working Directory";
            this.btnWorkingDirectory.UseVisualStyleBackColor = true;
            this.btnWorkingDirectory.Click += new System.EventHandler(this.btnWorkingDirectory_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsWorkingDirectory});
            this.statusStrip1.Location = new System.Drawing.Point(0, 401);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(830, 22);
            this.statusStrip1.TabIndex = 24;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsWorkingDirectory
            // 
            this.tsWorkingDirectory.Name = "tsWorkingDirectory";
            this.tsWorkingDirectory.Size = new System.Drawing.Size(103, 17);
            this.tsWorkingDirectory.Text = "Working Directory";
            // 
            // RelayStationDeviceStatusDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 423);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnWorkingDirectory);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbDevices);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.listBox1);
            this.Name = "RelayStationDeviceStatusDisplay";
            this.Text = "Relay Status Display";
            this.Load += new System.EventHandler(this.RelayStationDeviceStatusDisplay_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPin1;
        private System.Windows.Forms.Button btnPin2;
        private System.Windows.Forms.Button btnPin3;
        private System.Windows.Forms.Button btnPin4;
        private System.Windows.Forms.Button btnPin5;
        private System.Windows.Forms.Button btnPin6;
        private System.Windows.Forms.Button btnPin7;
        private System.Windows.Forms.Button btnPin8;
        private System.Windows.Forms.Button btnPin9;
        private System.Windows.Forms.Button btnPin10;
        private System.Windows.Forms.Button btnPin11;
        private System.Windows.Forms.Button btnPin12;
        private System.Windows.Forms.Button btnPin13;
        private System.Windows.Forms.Button btnPin14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.ComboBox cmbDevices;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label txtDevice;
        private System.Windows.Forms.Button btnWorkingDirectory;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsWorkingDirectory;
    }
}