﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HomeControl.Models;
using nControl.Models;

namespace HomeControl
{
    public partial class RelayStationDeviceStatusDisplay : Form
    {
        Timer t;
        string workingDirectory = @"/home/santonio/Development/git/nbroker/nUDP/bin/Debug/";

        private List<Device> deviceList;

        private List<RelayIOPin> currentRelayDevice;

        private Dictionary<int, Button> PinButtonMap;

        public RelayStationDeviceStatusDisplay()
        {
            InitializeComponent();
        }

        private void RelayStationDeviceStatusDisplay_Load(object sender, EventArgs e)
        {
            workingDirectory = @"/home/santonio/Development/git/nbroker/nUDP/bin/Debug/";

            PinButtonMap = new Dictionary<int, Button>();
            PinButtonMap.Add(14, btnPin1);
            PinButtonMap.Add(15, btnPin2);
            PinButtonMap.Add(16, btnPin3);
            PinButtonMap.Add(17, btnPin4);
            PinButtonMap.Add(18, btnPin5);
            PinButtonMap.Add(19, btnPin6);
            PinButtonMap.Add(20, btnPin7);
            PinButtonMap.Add(21, btnPin8);
            PinButtonMap.Add(22, btnPin9);
            PinButtonMap.Add(23, btnPin10);
            PinButtonMap.Add(24, btnPin11);

            //not sure what the remaining pins are

            //hook up events
            btnPin1.Click += btnSendMessage;
            btnPin2.Click += btnSendMessage;
            btnPin3.Click += btnSendMessage;
            btnPin4.Click += btnSendMessage;
            btnPin5.Click += btnSendMessage;
            btnPin6.Click += btnSendMessage;
            btnPin7.Click += btnSendMessage;
            btnPin8.Click += btnSendMessage;
            btnPin9.Click += btnSendMessage;
            btnPin10.Click += btnSendMessage;
            btnPin11.Click += btnSendMessage;


            t = new Timer();
            t.Interval = 500;
            t.Tick += (o, s) =>
            {
                LoadDevices();
                LoadDevice(cmbDevices.SelectedText);

                UpdateUi();
            };

            t.Enabled = true;
            t.Start();

            listBox1.Items.Add("Loaded");
        }

        //Load a specific relay
        private void LoadDevice(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                return;

            currentRelayDevice = Models.Relay.Load(workingDirectory + text + ".xml");
        }

        private void LoadDevices()
        {
            if(System.IO.File.Exists(workingDirectory + "Devices.xml"))
            {
                deviceList = Models.Device.Load(workingDirectory + "Devices.xml");
            }
            else
            {
                listBox1.Items.Add("Could not find Devices.xml");
            }
        }

        private void UpdateUi()
        {
            if(deviceList != null && cmbDevices.Items.Count != deviceList.Count)
            {
                listBox1.Items.Add("Found " + deviceList.Count + " devices");
                cmbDevices.Items.Clear();
                foreach (var d in deviceList)
                {
                    cmbDevices.Items.Add(d.GUID);
                }                
            }

            if(currentRelayDevice != null)
            {
                //update button states
                txtDevice.Text = cmbDevices.Text;

                foreach(var p in currentRelayDevice.OrderBy(a => a.PinNumber))
                {
                    //14-24 = (1-7,8-11/14)
                    UpdateButton(p);
                }
            }

            if (tsWorkingDirectory.Text != workingDirectory)
                tsWorkingDirectory.Text = workingDirectory;
        }

        private void UpdateButton(RelayIOPin p)
        {
            if (!PinButtonMap.ContainsKey(p.PinNumber))
            {
                Console.WriteLine("No mapping for pin " + p.PinNumber);
                return;
            }

            Button b = PinButtonMap[p.PinNumber];

            if (p.IsOn)
            {
                //listBox1.Items.Add(p.PinNumber + " " + p.PinName + " is on");
                b.BackColor = Color.Green;
            }
            else
            {
                b.BackColor = Color.LightGray;
            }

        }

        private void btnWorkingDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = workingDirectory;

            if(fbd.ShowDialog() == DialogResult.OK)
            {
                workingDirectory = fbd.SelectedPath;
                tsWorkingDirectory.Text = workingDirectory;

                LoadDevice(workingDirectory);
                UpdateUi();
            }
        }

        private void cmbDevices_SelectedValueChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(cmbDevices.SelectedText))
                return;

            listBox1.Items.Add("Selected new device " + cmbDevices.SelectedText);
            LoadDevice(cmbDevices.SelectedText);
        }

        private void btnSendMessage(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b == null)
                return;

            var pinNumber = PinButtonMap.Where(a => a.Value == b).First();

            int pin = pinNumber.Key;

            if (b.BackColor == Color.Green)
            {
                SimpleUDPServer.SendCommand("/SERVER/W/PIN_OFF/" + pin);
                listBox1.Items.Add("Send pin OFF to " + pin);
            }
            else
            {
                SimpleUDPServer.SendCommand("/SERVER/W/PIN_ON/" + pin);
                listBox1.Items.Add("Send pin ON to " + pin);
            }

            //ask device to send pin state update after its processed the message
            (new System.Threading.Thread(() =>
            {
                System.Threading.Thread.Sleep(1000);
                SimpleUDPServer.SendCommand("/SERVER/R/PIN_STATE/" + pin);

                if (listBox1.InvokeRequired)
                {
                    Invoke(new Action(() =>
                        {
                            listBox1.Items.Add("Request pin state " + pin);
                        })
                    );
                }
            })
            { IsBackground = true }).Start();

        }

        private void btnSend_Click(object sender, EventArgs e)
        {

        }
    }
}
