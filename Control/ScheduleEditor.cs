﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeControl
{
    public partial class ScheduleEditor : Form
    {
        public DayOfWeek dayOfWeek { get { return GetDayOfWeek(cmbDayOfWeek); } set { cmbDayOfWeek.Text = value.ToString(); } }
        public DateTime start { get { return deTime.Value; } set { deTime.Value = value; } }
        public int pinNumber { get { return Convert.ToInt32(cmbPin.Text); } set {cmbPin.Text = value.ToString(); } }
        public TimeSpan duration { get { return new TimeSpan(0, (int)spin.Value, 0); } set { spin.Value = (decimal)value.TotalMinutes; } }
        public string DeviceId { get { return cmbDevice.Text; } set { cmbDevice.Text = value.ToString(); } }
        public bool IsMaster { get { return chkIsMaster.Checked; } set { chkIsMaster.Checked = value; } }

        public ScheduleEditor()
        {
            InitializeComponent();

            ToolTip mToolTip = new ToolTip();
            mToolTip.Show("Master station will be on when ever another station is on", chkIsMaster);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {         
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private DayOfWeek GetDayOfWeek(ComboBox cmb)
        {
            if (cmb.Text.Trim() == DayOfWeek.Monday.ToString())
                return DayOfWeek.Monday;
            if (cmb.Text.Trim() == DayOfWeek.Tuesday.ToString())
                return DayOfWeek.Tuesday;
            if (cmb.Text.Trim() == DayOfWeek.Wednesday.ToString())
                return DayOfWeek.Wednesday;
            if (cmb.Text.Trim() == DayOfWeek.Thursday.ToString())
                return DayOfWeek.Thursday;
            if (cmb.Text.Trim() == DayOfWeek.Friday.ToString())
                return DayOfWeek.Friday;
            if (cmb.Text.Trim() == DayOfWeek.Saturday.ToString())
                return DayOfWeek.Saturday;
            if (cmb.Text.Trim() == DayOfWeek.Sunday.ToString())
                return DayOfWeek.Sunday;

            return DayOfWeek.Monday;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
