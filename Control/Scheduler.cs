﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HomeControl.Models;
using System.Runtime.InteropServices;
using nControl.Utilities;
using nControl.Models;

namespace HomeControl
{
    public partial class Scheduler : Form, IMessageFilter
    {
        private Models.WeeklySchedule schedule;

        //keep track of current pin, for new schedules
        static int currentPin = 1;
        //keep track of current time for new schedules
        static DateTime currentTime = new DateTime(2000, 1, 1, 3, 0, 0);

        //default is minutes per day
        static DateTime start = new DateTime(2000, 1, 1, 0, 0, 0);
        static DateTime end = new DateTime(2000, 1, 1, 23, 59, 59);

        static int duration() { return (int)(end - start).TotalMinutes; }

        static Label currentTimeLabel = new Label();

        static Server.SimpleServer server = new Server.SimpleServer();

        private int lastY = 0;
        private Timer uiUpdateTimer = new Timer();

        public Scheduler()
        {
            InitializeComponent();

            Init();
        }

        public Scheduler(bool Background)
        {
            InitializeComponent();

            if (!Background)
                Init();
            else
            {
                ConsoleHelper.WriteLine("Started Scheduler in Background", ConsoleColor.DarkGreen);

                start = nControl.Properties.Settings.Default.StartHour;
                end = nControl.Properties.Settings.Default.EndHour;

                LoadSchedule();

                SetupSimpleServer();
            }
        }

        private void Init()
        {
            Application.AddMessageFilter(this);

            this.MouseWheel += Scheduler_MouseWheel;

            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer,
                true);

            start = nControl.Properties.Settings.Default.StartHour;
            end = nControl.Properties.Settings.Default.EndHour;

            LoadSchedule();

            //select top device
            this.cmbDeviceList.Text = this.cmbDeviceList.Items[0].ToString();

            PrintGrid();

            SetupCurrentTimeMarker();

            SetupSimpleServer();

            //for middle mouse drag updates
            uiUpdateTimer.Interval = 200;
            uiUpdateTimer.Tick += (s, e) =>
            {
                RefreshForm();
            };
        }

        private void LoadSchedule()
        {
            try
            {
                schedule = new Models.WeeklySchedule();
                schedule.LoadFromDisk();

                ConsoleHelper.WriteLine("Loaded " + schedule.scheduleItems.Count + " scheduled items", ConsoleColor.DarkGreen);

                foreach (var s in schedule.scheduleItems)
                {
                    s.PropertyChanged += StationScheduleChange;
                }

                PopulateForm(schedule);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                //no schedule found
                PopulateForm(null);
            }
        }
      
        private void SetupSimpleServer()
        {
            //run the scheduler here for the network devices
            server.Start(ref schedule);
        }

        private void SetupCurrentTimeMarker()
        {
            Timer t = new Timer();
            t.Interval = 60000;
            t.Start();
            t.Tick += (o, s) =>
            {
                DrawTimeLine();
            };
            DrawTimeLine();
        }

        private void DrawTimeLine()
        {
            double heightOfDay = groupBox8.Size.Height;
            double heightScale = heightOfDay / duration();

            double posI = (DateTime.Now.Hour * 60) + DateTime.Now.Minute;

            double scaled = (posI) * heightScale;//1440 minutes in day

            currentTimeLabel = new Label();
            currentTimeLabel.Text = string.Empty;
            currentTimeLabel.BorderStyle = BorderStyle.None;
            currentTimeLabel.AutoSize = false;
            currentTimeLabel.Height = 2;
            currentTimeLabel.BackColor = Color.Red;
            currentTimeLabel.ForeColor = Color.Red;

            //offset from the top
            scaled += groupBox1.Location.Y;

            currentTimeLabel.Location = new Point(2, (int)scaled);
            currentTimeLabel.Width = this.Width - 4;
            currentTimeLabel.BringToFront();

            if (!this.Controls.Contains(currentTimeLabel))
                this.Controls.Add(currentTimeLabel);

            //todo add scrollable zoom
            ClearSelectedDay();

            switch (DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    groupBox1.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Tuesday:
                    groupBox2.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Wednesday:
                    groupBox3.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Thursday:
                    groupBox4.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Friday:
                    groupBox5.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Saturday:
                    groupBox6.BackColor = Color.LightCyan;
                    break;
                case DayOfWeek.Sunday:
                    groupBox7.BackColor = Color.LightCyan;
                    break;
            }
        }

        private void ClearSelectedDay()
        {
            groupBox1.BackColor = SystemColors.Control;
            groupBox2.BackColor = SystemColors.Control;
            groupBox3.BackColor = SystemColors.Control;
            groupBox4.BackColor = SystemColors.Control;
            groupBox5.BackColor = SystemColors.Control;
            groupBox6.BackColor = SystemColors.Control;
            groupBox7.BackColor = SystemColors.Control;
        }

        private void PrintGrid()
        {
            //clear
            groupBox8.Controls.Clear();

            //display time grid
            int posI = 0;
            for (int i = start.Hour; i <= end.Hour; i++)
            {
                //i = hour
                Label lbl = new Label();
                lbl.Text = String.Format("{00}", i) + ":00";
                lbl.AutoSize = true;
                lbl.Parent = groupBox8;
                lbl.Width = groupBox8.Width - 4;

                double heightOfDay = groupBox8.Size.Height;

                double heightScale = heightOfDay / duration();

                double scaled = (posI * 60) * heightScale;//1440 minutes in day

                //double offset = 60.0 * heightOfDay / duration() / 2.0;

                lbl.Location = new Point(5, (int)scaled);

                groupBox8.Controls.Add(lbl);

                //draw line
                //Label line = new Label();
                //line.Text = string.Empty;
                //line.BorderStyle = BorderStyle.Fixed3D;
                //line.AutoSize = false;
                //line.Height = 2;

                ////offset from the top
                //scaled += groupBox1.Location.Y;

                //line.Location = new Point(2, (int)scaled);
                //line.Width = this.Width - 4;
                //line.BringToFront();

                //this.Controls.Add(line);


                posI++;
            }
        }

        private void PositionButton(StationSchedule scheduleItem, Button btn)
        {
            double durationInMinutes = scheduleItem.Duration.TotalMinutes;

            double heightOfDay = groupBox1.Size.Height;
            double heightScale = heightOfDay / duration();

            //minutes from start
            var tempStart = new DateTime(scheduleItem.startTime.Year, scheduleItem.startTime.Month, scheduleItem.startTime.Day, start.Hour, start.Minute, start.Second);
            var minFromStart = scheduleItem.startTime - tempStart;

            double position = minFromStart.TotalMinutes * heightScale;

            if (scheduleItem.IsMaster)
            {
                btn.Location = new Point(2 + (groupBox1.Width / 2), (int)position);
            }
            else
            {
                btn.Location = new Point(2, (int)position);
            }

            var height = durationInMinutes / duration() * heightOfDay;

            btn.Height = (int)height;
        }

        private void PopulateForm(WeeklySchedule schedule)
        {
            LoadDevices();

            PrintGrid();

            if (schedule == null)
                return;

            foreach (var s in schedule.scheduleItems)
            {
                //put in buttons scaled to 24/hour period
                DisplayAppointment(s);
            }
        }
        private void StationScheduleChange(object sender, PropertyChangedEventArgs e)
        {
            /*var s = sender as Models.StationSchedule;
            if(s != null)
                DisplayAppointment(s);*/
            RemoveButtons();
            PopulateForm(schedule);
        }

        private void LoadDevices()
        {
            var devices = Models.Device.Load();

            if (devices == null || devices.Count == 0)
                return;

            foreach (var d in devices)
            {
                cmbDeviceList.Items.Add(d.GUID);
            }
        }

        private void DisplayAppointment(StationSchedule schedule)
        {
            Button btn = BuildButton(schedule);

            #region Schedule Button Events
            btn.MouseHover += (s, e) =>
            {
                ToolTip mToolTip = new ToolTip();
                mToolTip.Show("Start: " + schedule.startTime.ToString("HH:mm") + " End: " + (schedule.startTime + schedule.Duration).ToString("HH:mm") + " Duration: " + schedule.Duration.TotalMinutes, btn);
            };
            btn.Click += (s, e) =>
            {
                ScheduleEditor se = new ScheduleEditor();
                se.duration = schedule.Duration;
                se.start = schedule.startTime;
                se.dayOfWeek = schedule.day;
                se.pinNumber = schedule.PinNumber;
                se.DeviceId = schedule.DeviceId;
                se.IsMaster = schedule.IsMaster;

                if (se.ShowDialog() == DialogResult.OK)
                {
                    schedule.Duration = se.duration;
                    schedule.startTime = se.start;
                    schedule.day = se.dayOfWeek;
                    schedule.PinNumber = se.pinNumber;
                    schedule.DeviceId = se.DeviceId;
                    schedule.IsMaster = se.IsMaster;

                    //reset defaults
                    currentPin = se.pinNumber + 1;
                    currentTime = se.start.AddMinutes(15);

                    //refresh display
                    RemoveButton(btn);
                    PositionButton(schedule, btn);
                    AddButton(btn, schedule.day);
                }
            };
            /*btn.MouseDown += (s, e) =>
            {
                if(e.Button == MouseButtons.Left)
                {
                    Button b = s as Button;

                    if(e.Location - b.Location)
                }
            };*/

            #endregion

            PositionButton(schedule, btn);
            AddButton(btn, schedule.day);
        }

        private Button BuildButton(StationSchedule scheduleItem)
        {
            Button btn = new Button();

            //btn.Tag = scheduleItem;

            if (scheduleItem.DeviceId.Length >= 4)
                btn.Text = scheduleItem.DeviceId.Substring(0, 4) + ":" + scheduleItem.PinNumber.ToString();
            else
                btn.Text = scheduleItem.DeviceId + ":" + scheduleItem.PinNumber.ToString();
            btn.AutoSize = false;
            btn.BackColor = PinColors.GetColor(scheduleItem.PinNumber);
            btn.FlatStyle = FlatStyle.Flat;
            btn.Width = groupBox1.Width / 2;
            btn.Cursor = Cursors.Hand;

            return btn;
        }

        private void RemoveButton(Button btn)
        {
            //find in group boxs
            if (groupBox1.Controls.Contains(btn))
                groupBox1.Controls.Remove(btn);
            if (groupBox2.Controls.Contains(btn))
                groupBox2.Controls.Remove(btn);
            if (groupBox3.Controls.Contains(btn))
                groupBox3.Controls.Remove(btn);
            if (groupBox4.Controls.Contains(btn))
                groupBox4.Controls.Remove(btn);
            if (groupBox5.Controls.Contains(btn))
                groupBox5.Controls.Remove(btn);
            if (groupBox6.Controls.Contains(btn))
                groupBox6.Controls.Remove(btn);
            if (groupBox7.Controls.Contains(btn))
                groupBox7.Controls.Remove(btn);

            btn = null;
        }

        private void AddButton(Button btn, DayOfWeek dow)
        {
            switch (dow)
            {
                case DayOfWeek.Monday:
                    btn.Width = GetButtonWidth(groupBox1.Width, groupBox1.Padding, btn);
                    btn.Parent = groupBox1;
                    groupBox1.Controls.Add(btn);
                    break;
                case DayOfWeek.Tuesday:
                    btn.Width = GetButtonWidth(groupBox2.Width, groupBox2.Padding, btn);
                    btn.Parent = groupBox2;
                    groupBox2.Controls.Add(btn);
                    break;
                case DayOfWeek.Wednesday:
                    btn.Width = GetButtonWidth(groupBox3.Width, groupBox3.Padding, btn);
                    btn.Parent = groupBox3;
                    groupBox3.Controls.Add(btn);
                    break;
                case DayOfWeek.Thursday:
                    btn.Width = GetButtonWidth(groupBox4.Width, groupBox4.Padding, btn);
                    btn.Parent = groupBox4;
                    groupBox4.Controls.Add(btn);
                    break;
                case DayOfWeek.Friday:
                    btn.Width = GetButtonWidth(groupBox5.Width, groupBox5.Padding, btn);
                    btn.Parent = groupBox5;
                    groupBox5.Controls.Add(btn);
                    break;
                case DayOfWeek.Saturday:
                    btn.Width = GetButtonWidth(groupBox6.Width, groupBox6.Padding, btn);
                    btn.Parent = groupBox6;
                    groupBox6.Controls.Add(btn);
                    break;
                case DayOfWeek.Sunday:
                    btn.Width = GetButtonWidth(groupBox7.Width, groupBox7.Padding, btn);
                    btn.Parent = groupBox7;
                    groupBox7.Controls.Add(btn);
                    break;
            }
        }

        private int GetButtonWidth(int width, Padding padding, Button btn)
        {
            return (width - (padding.Left + padding.Right)) / 2;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            ScheduleEditor sef = new ScheduleEditor();
            sef.duration = new TimeSpan(0, 15, 0);
            sef.start = currentTime;
            sef.dayOfWeek = DayOfWeek.Monday;
            sef.pinNumber = currentPin;
            sef.DeviceId = cmbDeviceList.Text.Trim();
            sef.IsMaster = false;

            //next time we load pick a higher pin
            if (currentPin <= 14)
                currentPin++;
            if (currentPin > 14)
                currentPin = 1;

            currentTime += sef.duration;

            if (sef.ShowDialog() == DialogResult.OK)
            {
                var ss = new StationSchedule()
                {
                    Duration = sef.duration,
                    startTime = sef.start,
                    day = sef.dayOfWeek,
                    DeviceId = sef.DeviceId,
                    PinNumber = sef.pinNumber,
                    IsMaster = sef.IsMaster
                };

                ss.PropertyChanged += StationScheduleChange;

                this.schedule.scheduleItems.Add(ss);

                var btn = BuildButton(ss);

                #region Schedule Button Events
                btn.MouseHover += (s, e1) =>
                {
                    ToolTip mToolTip = new ToolTip();
                    mToolTip.Show("Start: " + sef.start.ToString("HH:mm") + " End: " + (sef.start + sef.duration).ToString("HH:mm") + " Duration: " + sef.duration.TotalMinutes, btn);
                };
                btn.Click += (s, e1) =>
                {
                    //display the clicked station schedule
                    ScheduleEditor se = new ScheduleEditor();
                    se.duration = sef.duration;
                    se.start = sef.start;
                    se.dayOfWeek = sef.dayOfWeek;
                    se.pinNumber = sef.pinNumber;
                    se.DeviceId = sef.DeviceId;
                    se.IsMaster = sef.IsMaster;

                    if (se.ShowDialog() == DialogResult.OK)
                    {
                        sef.duration = se.duration;
                        sef.start = se.start;
                        sef.dayOfWeek = se.dayOfWeek;
                        sef.pinNumber = se.pinNumber;
                        sef.DeviceId = se.DeviceId;
                        sef.IsMaster = se.IsMaster;

                        //reset defaults
                        currentPin = se.pinNumber + 1;
                        currentTime = se.start.AddMinutes(15);

                        //refresh display
                        RemoveButton(btn);
                        PositionButton(ss, btn);
                        AddButton(btn, sef.dayOfWeek);
                    }
                };
                #endregion

                //refresh display
                PositionButton(ss, btn);
                AddButton(btn, sef.dayOfWeek);
            }
        }

        private void RefreshForm()
        {
            PrintGrid();
            RemoveButtons();
            ClearSelectedDay();//remove the selected day and time line
            DrawTimeLine();//redraw with correct scaling

            foreach (var s in schedule.scheduleItems)
            {
                //put in buttons scaled to 24/hour period
                DisplayAppointment(s);
            }
        }

        private void RemoveButtons()
        {
            groupBox1.Controls.Clear();
            groupBox2.Controls.Clear();
            groupBox3.Controls.Clear();
            groupBox4.Controls.Clear();
            groupBox5.Controls.Clear();
            groupBox6.Controls.Clear();
            groupBox7.Controls.Clear();
        }

        #region Events

        #region scroll wheel
        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == 0x20a)
            {
                // WM_MOUSEWHEEL, find the control at screen position m.LParam
                Point pos = new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16);
                IntPtr hWnd = WindowFromPoint(pos);
                if (hWnd != IntPtr.Zero && hWnd != m.HWnd && Control.FromHandle(hWnd) != null)
                {
                    SendMessage(hWnd, m.Msg, m.WParam, m.LParam);
                    return true;
                }
            }
            return false;
        }

        // P/Invoke declarations
        [DllImport("user32.dll")]
        private static extern IntPtr WindowFromPoint(Point pt);
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
        #endregion

        private void Scheduler_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                this.Cursor = Cursors.SizeNS;
                uiUpdateTimer.Start();
                if (lastY != 0)
                {
                    var diff = (lastY - e.Y);

                    start = start.AddMinutes(diff);
                    end = end.AddMinutes(diff);


                    RefreshForm();
                }

                lastY = e.Y;
            }
        }

        private void Scheduler_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                lastY = 0;
                uiUpdateTimer.Stop();
                this.Cursor = Cursors.Default;
                uiUpdateTimer.Stop();
            }
        }

        private void btnChangeDisplayRange_Click(object sender, EventArgs e)
        {
            TimeRangeView form = new TimeRangeView();
            form.Start = start;
            form.End = end;
            if (form.ShowDialog() == DialogResult.OK)
            {
                start = form.Start;
                end = form.End;

                RefreshForm();
            }
        }

        private void Scheduler_FormClosed(object sender, FormClosedEventArgs e)
        {
            schedule.SaveToDisk();

            nControl.Properties.Settings.Default.StartHour = start;
            nControl.Properties.Settings.Default.EndHour = end;
            //nControl.Properties.Settings.Default.WorkingDirectory = workingDirectory;

            nControl.Properties.Settings.Default.Save();
        }

        private void Scheduler_MouseWheel(object sender, MouseEventArgs e)
        {
            //move based upon mouse Y location
            var scaleStart = ((decimal)e.Y / (decimal)this.Height);
            var scaleEnd = 1 - ((decimal)e.Y / (decimal)this.Height);

            //flat zoom
            var startMinute = (decimal)e.Delta / 2.0M * scaleStart;
            start = start.AddMinutes((int)startMinute);

            var endMinute = (decimal)-e.Delta / 2.0M * scaleEnd;
            end = end.AddMinutes((int)endMinute);

            //refresh
            RefreshForm();
        }

        #endregion
    }
}
