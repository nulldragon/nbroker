﻿using nControl.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeControl.Server
{
    public class SimpleServer
    {
        private static Models.WeeklySchedule schedule;

        public void Start(ref Models.WeeklySchedule weeklySchedule)
        {
            SimpleServer.schedule = weeklySchedule;

            SimpleUDPServer.Start();

            ConsoleHelper.WriteLine("Started SimpleUDPServer", ConsoleColor.DarkGreen);

            //setup a listener
            Timer t = new Timer();
            t.Interval = 10000;
            t.Tick += (s, e) =>
            {
                //SimpleServer.schedule.scheduleItems.First().DeviceId = "GU" + DateTime.Now.Second;//testing if we can postback to the GUI (we can)
                ProcessMessages();

                SendMessages();
            };
            t.Start();
            //run periodic schedule updates
        }

        private static void SendMessages()
        {
            foreach(var s in schedule.scheduleItems)
            {
                if (s.IsOn())
                {
                    ConsoleHelper.WriteLine("UDP -> " + s.DeviceId + "/" + s.PinNumber + "/ON", ConsoleColor.DarkGreen);
                    SimpleUDPServer.SendCommand(s.DeviceId + "/PIN_ON/" + s.PinNumber);
                }
                else
                {
                    ConsoleHelper.WriteLine("UDP -> " + s.DeviceId + "/" + s.PinNumber + "/OFF", ConsoleColor.DarkGreen);
                    SimpleUDPServer.SendCommand(s.DeviceId + "/PIN_OFF/" + s.PinNumber);
                }
                System.Threading.Thread.Sleep(250);
            }
        }

        private void ProcessMessages()
        {
            if (SimpleUDPServer.Messages.Count > 0)
            {
                var msg = SimpleUDPServer.Messages.Pop();

                //check if msg came from us
                if (GetLocalIPAddress().Contains(msg.Source))
                    return;//self message disregard

                //if this is a new device add it to our device list
                if(!Models.Device.Exists(msg))
                {
                    ConsoleHelper.WriteLine("MSG -> " + msg + " has not been seen before", ConsoleColor.Yellow);
                    Models.Device.Add(msg);
                }
                else
                {
                    //device exists in our list, update last seen
                    Models.Device.Update(msg);
                }

                Console.WriteLine(msg);
            }
        }

        public static List<IPAddress> GetLocalIPAddress()
        {
            List<IPAddress> address = new List<IPAddress>();

            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                address.Add(ip);

            }
            return address;
        }
    }
}
