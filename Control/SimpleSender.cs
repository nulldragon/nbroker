﻿using nControl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HomeControl
{
    public class SimpleUDPServer
    {
        public static int Port = 15000;
        private static readonly UdpClient udp = new UdpClient(Port);

        public static volatile Stack<nMessage> Messages = new Stack<nMessage>();
        
        public static  void SendCommand(string v)
        {
            var data = Encoding.ASCII.GetBytes(v);

            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Port);//broadcast message
            udp.Send(data, data.Length, ip);

        }

        public static void Start()
        {
            udp.BeginReceive(Receive, null);
        }

        private static void Receive(IAsyncResult ar)
        {
            (new System.Threading.Thread(() =>
            {
                Start();
            })).Start();

            //thread the message processing out as it shouldn't block our server
            //(new System.Threading.Thread(() =>
            //{
                //IPEndPoint ip = new IPEndPoint(IPAddress.Any, Port);
                IPEndPoint ip = new IPEndPoint(IPAddress.Any, 0);//Linux any port (but the udp object already is filtered)
                byte[] bytes = udp.EndReceive(ar, ref ip);


                string message = string.Empty;
                foreach (var b in bytes)
                {
                    message += (char)b;
                }

                Messages.Push(new nMessage()
                {
                    Message = message,
                    Source = ip.Address
                });
                
            //})
            //{ IsBackground = true }).Start();


        }
        
    }
}
