﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nControl.Utilities
{
    public class ConsoleHelper
    {
        public static void WriteLine(string message, ConsoleColor color)
        {
            var current = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(message);

            Console.ForegroundColor = current;
        }

    }
}
