﻿using System;
using System.Drawing;

namespace nControl.Utilities
{
    public static class PinColors
    {
        public static Color[] ourColor =
        {
            Color.FromArgb(179,158,181),
            Color.FromArgb(119,190,119),
            Color.FromArgb(119,158,203),
            Color.FromArgb(253,253,150),
            Color.FromArgb(255,105,97),
            Color.FromArgb(207,207,196),
            Color.FromArgb(174,198,207),
            Color.FromArgb(255,209,220),
            Color.FromArgb(150,111,214),
            Color.FromArgb(130,105,83),
            Color.FromArgb(222,165,164),
            Color.FromArgb(255,179,71),
            Color.FromArgb(100,20,100),
            Color.FromArgb(255,105,181),
        };

        public static Random r = new Random(DateTime.Now.Second);

        public static Color GetColor(int pinNumber)
        {
            //limit to how ever many colors in our array
            if (pinNumber < 0)
                pinNumber = 0;
            if (pinNumber > ourColor.Length - 1)
                pinNumber = r.Next(ourColor.Length - 1);

            return ourColor[pinNumber];
        }
    }
}
