﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Node
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Node v1");

            PrintHelp();

            Sender s = new Sender();
            s.SetupHeartbeat();

            ConsoleKeyInfo key;

            do
            {
                key =  Console.ReadKey();


                if (key.Key == ConsoleKey.R)
                {
                    Console.WriteLine("Request");
                    s.Request("DEBUG/Version");
                }
                else if(key.Key == ConsoleKey.I)
                {
                    Sender.deviceId++;
                    Console.WriteLine("Changed Device Id to " + Sender.deviceId);
                }
            } while (key.Key != ConsoleKey.Q);

        }

        static void PrintHelp()
        {
            Console.WriteLine("Q to quit");
        }
    }
}
