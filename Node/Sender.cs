﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Node
{
    public class Sender
    {
        public static int Port = 15000;
        public static UInt16 deviceId = 23;
        public static byte version = 1;

        System.Timers.Timer beatTimer;

        public void Send()
        {
            UdpClient client = new UdpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Port);
            byte[] bytes = Encoding.ASCII.GetBytes("DEBUG");
            client.Send(bytes, bytes.Length, ip);
            client.Close();
        }

        public void Request(string topic)
        {
            UdpClient client = new UdpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Port);

            Packets.Request rqt = new Packets.Request();
            rqt.deviceId = Sender.deviceId;
            rqt.version = Sender.version;

            if (topic.Length > 255)
                throw new ArgumentException("Topic to large");

            rqt.topicLength = (byte)topic.Length;
            rqt.topic = topic;

            var bytes = Packets.Request.getBytes(rqt);

            client.Send(bytes, bytes.Length, ip);
            client.Close();
        }

        public void SetupHeartbeat()
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Port);

            beatTimer = new System.Timers.Timer();
            beatTimer.AutoReset = true;
            beatTimer.Interval = 15000;
            beatTimer.Elapsed += (s, e) =>
            {
                UdpClient client = new UdpClient();

                Packets.Heartbeat beat = new Packets.Heartbeat();
                beat.beatRate = (byte)(beatTimer.Interval / 1000);
                beat.deviceId = Sender.deviceId;
                beat.version = Sender.version;

                byte[] bytes = Packets.Heartbeat.getBytes(beat);

                client.Send(bytes, bytes.Length, ip);
                client.Close();

                Console.WriteLine("Beat");
            };
            beatTimer.Start();
        }

    }
}
