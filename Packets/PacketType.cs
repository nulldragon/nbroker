﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    public class PacketType
    {
        public static Type GetPacketType(ref byte[] packet)
        {
            if (packet.Length <= 0)
                throw new ArgumentException("packet length zero");

            switch(packet[0])
            {
                case 0x01:
                    return typeof(Heartbeat);
                case 0x02:
                    return typeof(HeartACK);
                case 0x03:
                    return typeof(Request);
                case 0x04:
                    return typeof(RequestResponse);
                case 0x05:
                case 0x06:
                default:
                    return typeof(object);
            }
        }        
    }
}
