﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct HeartACK
    {
        #region Packet Data
        private byte packetType;
        public UInt16 serverId;
        public byte version;
        #endregion

        public static byte PacketType = 0x02;

        public static byte[] getBytes(HeartACK str)
        {
            str.packetType = HeartACK.PacketType;

            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static HeartACK fromBytes(byte[] arr)
        {
            HeartACK str = new HeartACK();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (HeartACK)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }
    }
}
