﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Heartbeat
    {
        #region Packet Data
        private byte packetType;
        //todo encryption type
        public UInt16 deviceId;
        public byte beatRate;
        public byte version;
        #endregion

        public static byte PacketType = 0x01;
        
        public static byte[] getBytes(Heartbeat str)
        {
            str.packetType = Heartbeat.PacketType;

            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static Heartbeat fromBytes(byte[] arr)
        {
            Heartbeat str = new Heartbeat();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (Heartbeat)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }
    }
}
