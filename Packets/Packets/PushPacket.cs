﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PushPacket
    {
        private byte packetType;
        public UInt16 deviceId;
        public byte topicLength;
        public byte payloadLength;
        public byte version;
        public UInt16 packetCount;
        public string topic;
        public string payload;
    }
}
