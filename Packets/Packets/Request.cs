﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Request
    {
        #region Packet Data
        private byte packetType;
        public UInt16 deviceId;
        public byte topicLength;
        public byte version;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 255)]
        public string topic;
        #endregion

        public static byte PacketType = 0x03;

        public static byte[] getBytes(Request str)
        {
            str.packetType = Request.PacketType;

            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static Request fromBytes(byte[] arr)
        {
            Request str = new Request();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (Request)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }
    }
}
