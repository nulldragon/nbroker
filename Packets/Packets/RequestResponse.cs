﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Packets
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RequestResponse
    {
        #region Packet Data
        private byte packetType;
        public UInt16 serverId;
        public byte status;
        public byte version;
        public UInt16 requestKeyId;
        #endregion

        public static byte PacketType = 0x04;

        public static byte[] getBytes(RequestResponse str)
        {
            str.packetType = RequestResponse.PacketType;

            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static RequestResponse fromBytes(byte[] arr)
        {
            RequestResponse str = new RequestResponse();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (RequestResponse)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }
    }
}
