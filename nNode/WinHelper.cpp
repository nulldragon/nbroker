
#include "WinHelper.h"


void delay(unsigned milliseconds)
{
	Sleep(milliseconds);
}

clock_t clock_begin = clock();
uint32_t millis()
{
	clock_t end = clock();
	double elapsed_msecs = double(end - clock_begin) / CLOCKS_PER_SEC * 1000;

	return (uint32_t)elapsed_msecs;
}
