#pragma once

#ifndef _WINHELPER_H_
#define _WINHELPER_H_

#ifdef _MSC_VER

#include <string>
#include "WindowsSerial.h"

#include <windows.h>
#include <ctime>

#define HardwareSerial Serial

void delay(unsigned milliseconds);
uint32_t millis();

class String {
public:
	String() {}
	String(const char* cp) { this->buffer = std::string(cp); }
	String(String &stz) { this->buffer = stz.buffer; }
	String(std::string stz) { this->buffer = stz; }

	int toInt() { return 0; }

	int indexOf(String &stz) { return this->buffer.find(stz.buffer); }
	int indexOf(const char* cp) { return this->buffer.find(cp); }
	int indexOf(const char* cp, int start) { return this->buffer.find(cp, start); }
	int indexOf(char c, int start) { return this->buffer.find(c,start); }

	String substring(int32_t index) { return String(this->buffer.substr(index)); }
	String substring(int32_t s , int32_t e) { return String(this->buffer.substr(s,e)); }

	const char* c_str() { return buffer.c_str(); }

	int length() { return (int)buffer.length(); }

	String operator+=(const String& rhs) 
	{                          
		this->buffer += rhs.buffer;
		return *this; // return the result by reference
	}

	String operator+=(const char rhs)
	{
		this->buffer += rhs;
		return *this; // return the result by reference
	}

	String operator+=(const char* rhs)
	{
		this->buffer += std::string(rhs);
		return *this; // return the result by reference
	}
private:

	std::string buffer;
};


#endif

#endif //_WINHELPER_H_