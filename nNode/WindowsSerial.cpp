
#include "WindowsSerial.h"


Serial::Serial()
{
	this->iColor = 8;
	this->iPort = DEFAULT_PORT;

	this->iBufferPosition = 0;
	this->iBufferReadPosition = 0;

	this->SetupListener();
}

Serial::Serial(int color)
{
	this->iColor = color;
	this->iPort = DEFAULT_PORT;

	this->iBufferPosition = 0;
	this->iBufferReadPosition = 0;

	this->SetupListener();
}


bool Serial::SetupListener()
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		std::cerr << "WSAStartup failed with error: " << iResult << "\r\n";
		return false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_UDP;

	// Resolve the server address and port
	iResult = getaddrinfo("localhost", "15005", &hints, &result);
	if (iResult != 0) {
		std::cerr << "getaddrinfo failed with error: " << iResult << "\r\n";
		WSACleanup();
		return false;
	}
}

void Serial::println(const char* msg)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, this->iColor);
	std::cout << msg << "\r\n";
}

/*void Serial::println(String stz)
{
	println(stz.c_str());
}*/

void Serial::println(uint8_t n)
{
	std::string s = std::to_string(n);
	println(s.c_str());
}

void Serial::print(const char* msg)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, this->iColor);
	std::cout << msg;
}

void Serial::print(uint8_t n)
{
	std::string s = std::to_string(n);
	print(s.c_str());
}

/*void Serial::print(String stz)
{
	print(stz.c_str());
}*/

void Serial::begin(int)
{
//meh do nothing in windows
}

int Serial::available()
{
	return (this->iBufferPosition-this->iBufferReadPosition);
}

byte Serial::read()
{
	byte result = this->bBuffer[this->iBufferReadPosition];
	this->iBufferReadPosition++;
	if (this->iBufferReadPosition >= SERIAL_BUFFER_SIZE - 1)
	{
		this->iBufferReadPosition = 0;
	}
	return result;
}

void Serial::write(byte b)
{
	
	this->bBuffer[this->iBufferPosition] = b;
	this->iBufferPosition++;

	if(this->iBufferPosition >= SERIAL_BUFFER_SIZE -1)
	{
		this->iBufferPosition = 0;
	}
}
