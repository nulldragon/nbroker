#pragma once

#ifndef _WINSERIAL_H_
#define _WINSERIAL_H_

#include <iostream>

#include <winsock2.h>
#include <ws2tcpip.h>

#include <Windows.h>


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#include "WinHelper.h"

#define SERIAL_BUFFER_SIZE 16

//network listener (default port 15005)
#define DEFAULT_PORT 15005

class Serial {
	public:
		Serial();
		Serial(int color);

		//void println(String stz);
		void println(const char* msg);
		void println(uint8_t n);

		//void print(String stz);
		void print(const char* msg);
		void print(uint8_t n);

		void begin(int);
		int available();
		byte read();
		void write(byte);
	private:
		bool SetupListener();

		int iColor;

		int iPort;
		int iBufferPosition;
		int iBufferReadPosition;
		byte bBuffer[SERIAL_BUFFER_SIZE];
};



#endif //_WINSERIAL_H_
