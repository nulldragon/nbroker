#pragma once

typedef unsigned char byte;

#define _HEARTBEAT_TYPE_ 0x01

struct Heartbeat
{
	byte packetType = _HEARTBEAT_TYPE_;
	//todo encryption type
	__int16 deviceId;
	byte beatRate;
	byte version;

};