#pragma once

#include "Message.h"
#include "ESP8266.h"

#ifdef _MSC_VER 
#include "WindowsSerial.h"

using namespace std;
#endif


#define STACK_SIZE 16

class nTelemetryProtocol {
	public:
		nTelemetryProtocol();
		nTelemetryProtocol(ESP8266 &esp8266);/*  */
		nTelemetryProtocol(const char* deviceId, ESP8266 &esp8266);

		//send message from this deviceId
		bool BroadcastMessage(Message message);
		
		int CheckMessages();/* return how many messages are on the stack */
		Message PopMessage();/* get the top message off of the stack*/

		void Update();/* Runs wifi recv()*/
	private:

		char deviceId[8];
		Message messageStack[STACK_SIZE];
};




