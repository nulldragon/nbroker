﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace nUDP.Devices
{
    public class DeviceManager
    {
        public static List<Device> Devices = new List<Device>();

        static DeviceManager()
        {
            //load Devices from disk
            Devices = new List<Device>();

            LoadFromDisk();
        }
        public static bool SaveToDisk()
        {
            //try
            {
                string fn = AppDomain.CurrentDomain.BaseDirectory + "Devices.xml";
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Device>));

                using (var f = new FileStream(fn, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    var ValidDevices = (from d in DeviceManager.Devices
                                        where !String.IsNullOrWhiteSpace(d.GUID)
                                        select d).ToList();
                    ValidDevices = ValidDevices.Distinct(new DistinctItemComparer()).ToList();//remove dupliacted GUIDs

                    xmlSerializer.Serialize(f, ValidDevices);
                }
            }
            //catch (Exception ex)
            {
                //Console.WriteLine(ex);
                //return false;
            }

            return true;
        }

        public static bool LoadFromDisk()
        {
            string fn = AppDomain.CurrentDomain.BaseDirectory + "Devices.xml";
            if (File.Exists(fn))
            {
                FileInfo fi = new FileInfo(fn);
                if (fi.Length == 0)
                    return false;

                XmlSerializer xmlSerializer = new XmlSerializer(DeviceManager.Devices.GetType());

                using (var f = File.OpenRead(fn))
                {
                    var rp = xmlSerializer.Deserialize(f);
                    DeviceManager.Devices = rp as List<Device>;
                    DeviceManager.Devices = DeviceManager.Devices.Distinct(new DistinctItemComparer()).ToList();//remove dupliacted GUIDs

                    return true;
                }
            }
            return false;
        }
        
       
    }

    public class Device
    {
        public string GUID { get; set; }//master

        [XmlIgnore]
        public IPAddress IP { get; set; }

        [XmlElement("MasterIP")]
        public string MasterIPForXml
        {
            get
            {
                return IP == null ? string.Empty : IP.ToString();
            }
            set
            {
                IP = string.IsNullOrEmpty(value) ? null :
                IPAddress.Parse(value);
            }
        }

        public DateTime Created { get; set; }
        public DateTime LastSeen { get; set; }

        public DeviceType DeviceType { get; set; }

        public bool IsOnline()
        {
            Ping x = new Ping();
            PingReply reply = x.Send(this.IP);

            if (reply.Status == IPStatus.Success)
                return true;

            return false;
        }
    }

    class DistinctItemComparer : IEqualityComparer<Device>
    {
        public bool Equals(Device x, Device y)
        {
            return x.GUID == y.GUID &&
                x.DeviceType == y.DeviceType;
        }

        public int GetHashCode(Device obj)
        {
            return obj.GUID.GetHashCode() ^
                obj.DeviceType.GetHashCode();
        }
    }

    public enum DeviceType
    {
        RelayStation,
        Other,
        Unknown
    }
}
