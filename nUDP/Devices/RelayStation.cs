﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace nUDP.Devices
{
    public class RelayStation
    {
        public static List<Station> masterStations = null;

        public static string[] readCommands = {
            "ADVERTISE",
            "VERSION",
            "PIN_STATE",
            "COMMANDS",
            "CONNECTED",//what pin numbers are connected
        };

        public static string[] writeCommands =
        {
            "PIN_ON",
            "PIN_OFF",
            "PIN_STATE",
        };

        public RelayStation()
        {
            if (masterStations == null)
            {
                masterStations = new List<Station>();
                LoadStations();
            }
        }

        private void LoadStations()
        {
            foreach (var d in DeviceManager.Devices.Where(a => a.DeviceType == DeviceType.RelayStation))
            {
                //load from xml - todo
                Station st = new Station();
                //st.GUID = "70D9A386-9D32-46A6-910D-E029EF3B0E9E";
                st.GUID = d.GUID;

                if (!LoadDeviceSettings(ref st))
                {
                    BroadcastRequestForInfo(st.GUID);
                    //ask network who goes by this id and how many pins they control            

                    d.IP = st.IP;
                    if(d.IsOnline())
                    {
                        d.LastSeen = DateTime.Now;
                    }
                }

                masterStations.Add(st);
            }
        }

        private void BroadcastRequestForInfo(string GUID)
        {
            Listener.SendCommand("SERVER/R/State/" + GUID);
        }

        //schedule
        public bool LoadDeviceSettings(ref Station station)
        {
            Console.WriteLine("Loading Device Settings for " + station.GUID);
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + station.GUID + ".xml"))
            {
                station.Solenoids = Import(station.GUID + ".xml");
            }

            //ask for an ip address

            return true;
        }

        public void Save()
        {
            foreach(var d in masterStations)
            {
                Export(d.GUID + ".xml", d.Solenoids);
            }
        }

        private void BuildScheduleDoW(ref Pins st18, DayOfWeek dow, uint duration, ushort hour, ushort minute)
        {
            var s1 = new Schedule()
            {
                dayOfWeek = dow,
                durationMinutes = duration,
                scheduleType = ScheduleType.DayOfWeek,
                hour = hour,
                minute = minute
            };

            st18.schedule.Add(s1);
        }

        private Pins CreatePins(string pinName, int pinNumber)
        {
            Pins p = new Pins();

            p.PinName = pinName;
            p.PinNumber = pinNumber;
            p.schedule = new List<Schedule>();

            return p;
        }

        public void ProcessAdvertise(string deviceId,  IPEndPoint ip)
        {
            Console.WriteLine("Updating Station with Command List");

            //send relay one message if we have one scheduled
            var st = masterStations.Where(a => a.GUID == deviceId).First();
            if (st == null)
            {
                Console.WriteLine(deviceId + " has no settings");
                return;
            }

            if(st.Solenoids == null || st.Solenoids.Count == 0)
            {
                Listener.SendCommand(ip, "/SERVER/R/CONNECTED/");
                System.Threading.Thread.Sleep(500);
            }

            //if pin states have not been updated for the last minute lets get them again
            
            foreach (var p in st.Solenoids)
            {
                if (p.LastUpdate < DateTime.Now.AddMinutes(-2))
                {
#if DEBUG
                    var tfp = Console.ForegroundColor;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine( p.LastUpdate.ToString("yyyyMMdd HH:mm") + " < " + DateTime.Now.AddMinutes(-1).ToString("yyyyMMdd HH:mm"));
                    Console.ForegroundColor = tfp;
#endif

                    //server, requests, pin state, for solienoid pin
                    Listener.SendCommand(ip, "/SERVER/R/PIN_STATE/" + p.PinNumber);
                    System.Threading.Thread.Sleep(200);
                }
            }
            #region Scheduling
            Console.WriteLine(st.GUID + " processing");

            //move this into a threaded timer loop that is not triggered by the remote device
            foreach (var r in st.Solenoids)
            {
                //Console.WriteLine("Checking " + r.PinName + " schedules");
                foreach (var s in r.schedule)
                {
                    //Console.WriteLine("Found " + r.schedule.Count() + " schedules");
                    switch (s.scheduleType)
                    {
                        case ScheduleType.DayOfWeek:
                            if (s.dayOfWeek == DateTime.Now.DayOfWeek)
                            {
                                //Console.WriteLine("Matched Day of Week");

                                var start = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, s.hour, s.minute, 0);
                                var end = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, s.hour, s.minute, 0);
                                end = end.AddMinutes(s.durationMinutes);

                                //Console.WriteLine("Start: " + start.ToString("yyyyMMdd HH:mm:ss") + " current " + DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + " End: " + end.ToString("yyyyMMdd HH:mm:ss"));

                                if (DateTime.Now.Ticks > start.Ticks && DateTime.Now.Ticks < end.Ticks)
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    Console.WriteLine("Telling Device " + ip + " to turn on (" + r.PinNumber + ") " + r.PinName);
                                    Listener.SendCommand(ip, "/SERVER/W/PIN_ON/" + r.PinNumber);
                                    System.Threading.Thread.Sleep(1000);
                                    //check status of this pin
                                    Listener.SendCommand(ip, "/SERVER/R/PIN_STATE/" + r.PinNumber);
                                    System.Threading.Thread.Sleep(1000);

                                    //todo set all other pins to off if they are on.
                                }

                                //todo turn off other pins
                            }
                            break;
                        default:
                            Console.WriteLine("Schedule type is not implemented yet");
                            break;
                    }
                }
                
            }

            Console.WriteLine("Finished Updating Station");
            #endregion
        }

        public void ProcessConnected(string deviceId, IPEndPoint ip, string pins)
        {
#if DEBUG
            var tfp = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(deviceId + "\t" + pins + "Process Connected()");
            Console.ForegroundColor = tfp;
#endif

            var st = masterStations.Where(a => a.GUID == deviceId).First();
            if (st == null)
            {
                Console.WriteLine(deviceId + " has no settings");
                return;
            }

            List<Pins> old = new List<Pins>();

            if (st.Solenoids != null && st.Solenoids.Count > 0)
            {
                Console.WriteLine(st.GUID + " already had solinoids connected");
                old = st.Solenoids;
            }

            st.Solenoids = new List<Pins>();
            var p = pins.Split(',');
            foreach (var pin in p)
            {
                int pn = 0;
                if (int.TryParse(pin, out pn))
                {
                    //try get the old name
                    Pins name = old.Where(a => a.PinNumber == pn).First();

                    Pins sol = new Pins()
                    {
                        IsOn = false,
                        PinName = string.Empty,
                        PinNumber = pn,
                        schedule = new List<Schedule>()
                    };

                    if (name != null)
                        sol.PinName = name.PinName;

                    st.Solenoids.Add(sol);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="pin"></param>
        /// <param name="state">Unused</param>
        public void UpdatePinState(string device, string pin, string state)
        {
            var dev = masterStations.Where(a => a.GUID == device).First();

            //should not happen
            if (dev == null)
            {
                Console.WriteLine("Device not found??");
                return;
            }

            //pin in format [p-s]
            //p = pin
            //s = state
            //etg 18-0 
            //means pin 18 is off
            if(!pin.Contains('-'))
            {
                var fg = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Pin state in wrong format");
                Console.ForegroundColor = fg;
                return;
            }

            var pinStateData = pin.Split('-');

            bool pinState = pinStateData[1].Equals("1");

            int pinNumber = 0;
            int.TryParse(pinStateData[0], out pinNumber);

#if DEBUG
            var tfp = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(dev.GUID + "\tPin State Data: " + pinStateData[0] + " " + pinStateData[1]);
            Console.ForegroundColor = tfp;
#endif

            if (dev.Solenoids.Count == 0)
            {
                var fg = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("device " + dev.GUID + " has no attatched solenoids so we will add it");
                Console.ForegroundColor = fg;

                dev.Solenoids.Add(new Pins()
                {
                    IsOn = pinState,
                    PinNumber = pinNumber,
                    schedule = new List<Schedule>()
                });
            }

            var devPin = dev.Solenoids.FirstOrDefault(a => a.PinNumber == pinNumber);

            if(devPin == null)
            {
                var fg = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("device " + dev.GUID + " has no pin numbered " + pinNumber);
                Console.ForegroundColor = fg;
                return;
            }

            devPin.LastUpdate = DateTime.Now;
            devPin.IsOn = pinState;
        }

        #region Hard Coded
        public List<Pins> HardCodedStationExample()
        {
            //"70D9A386-9D32-46A6-910D-E029EF3B0E9E"           
            var RelayPins = new List<Pins>();

            var st18 = CreatePins("South East Row 1", 18);
            var st19 = CreatePins("South West Row 1", 19);
            var st20 = CreatePins("South Row 2", 20);
            var st21 = CreatePins("South Misc", 21);
            var st22 = CreatePins("East South Row 1", 22);
            var st23 = CreatePins("East North Row 1", 23);
            var st24 = CreatePins("East Row 2", 24);

            //schedules here
            BuildScheduleDoW(ref st18, DayOfWeek.Monday, 14, 5, 1);
            BuildScheduleDoW(ref st18, DayOfWeek.Friday, 14, 5, 1);

            BuildScheduleDoW(ref st19, DayOfWeek.Monday, 14, 5, 15);
            BuildScheduleDoW(ref st19, DayOfWeek.Friday, 14, 5, 15);

            BuildScheduleDoW(ref st20, DayOfWeek.Monday, 14, 5, 30);
            BuildScheduleDoW(ref st20, DayOfWeek.Friday, 14, 5, 30);

            BuildScheduleDoW(ref st21, DayOfWeek.Monday, 14, 5, 45);
            BuildScheduleDoW(ref st21, DayOfWeek.Friday, 14, 5, 45);

            BuildScheduleDoW(ref st22, DayOfWeek.Monday, 14, 6, 1);
            BuildScheduleDoW(ref st22, DayOfWeek.Friday, 14, 6, 1);

            BuildScheduleDoW(ref st23, DayOfWeek.Monday, 14, 6, 15);
            BuildScheduleDoW(ref st23, DayOfWeek.Friday, 14, 6, 15);

            BuildScheduleDoW(ref st24, DayOfWeek.Monday, 14, 6, 20);
            BuildScheduleDoW(ref st24, DayOfWeek.Friday, 14, 6, 20);

            //debug
            BuildScheduleDoW(ref st24, DayOfWeek.Friday, 3600, 1, 1);

            RelayPins.Add(st18);
            RelayPins.Add(st19);
            RelayPins.Add(st20);
            RelayPins.Add(st21);
            RelayPins.Add(st22);
            RelayPins.Add(st23);
            RelayPins.Add(st24);


            return RelayPins;
        }
        #endregion

        public void Export(string fn, List<Pins> pins)
        {
            fn = AppDomain.CurrentDomain.BaseDirectory + fn;
            XmlSerializer xmlSerializer = new XmlSerializer(pins.GetType());

            using (var f = new FileStream(fn, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlSerializer.Serialize(f, pins);
            }
        }

        /// <summary>
        /// Loads all Pins from disk.
        /// </summary>
        /// <param name="fn"></param>
        /// <returns></returns>
        public List<Pins> Import(string fn)
        {
            fn = AppDomain.CurrentDomain.BaseDirectory + fn;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof (List<Pins>));

            using (var f = File.OpenRead(fn))
            {
                var rp = xmlSerializer.Deserialize(f);
                return rp as List<Pins>;
            }
        }
    }

    public class Scheduler
    {
        public void Update()
        {
            throw new NotImplementedException();
        }

        public static bool Validate(List<Station> pins, bool fixUp = true)
        {
            return false;
        }
    }
    
    public class Station : Device
    {
        //public string GUID { get; set; }
        //public IPAddress IP { get; set; }
        public List<Pins> Solenoids { get; set; }
        public DateTime lastUpdated { get; set; }

        public Station()
        {
            GUID = "";
            IP = IPAddress.Any;
            Solenoids = new List<Pins>();
            lastUpdated = DateTime.MinValue;
        }
    }

    public class Pins
    {
        public int PinNumber;
        public string PinName;
        public List<Schedule> schedule;

        public bool IsOn;

        public DateTime LastUpdate;
    }

    public class Schedule
    {        
        //public DateTime? date;

        public DayOfWeek dayOfWeek;
        public UInt16 hour;
        public UInt16 minute;

        public uint durationMinutes;
        //public bool IsRecuring;
        public ScheduleType scheduleType;
    }

    public enum ScheduleType
    {
        DayOfWeek,
        Even,
        Odd
    }
}
