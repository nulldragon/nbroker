﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace nUDP
{
    public static class Listener
    {
        public static int Port = 15000;
        private static readonly UdpClient udp = new UdpClient(Port);
        public static void StartListening()
        {
            Console.WriteLine("Listening on " + Port);
            Listener.udp.BeginReceive(Receive, new object());
        }

        private static void Receive(IAsyncResult ar)
        {
            //IPEndPoint ip = new IPEndPoint(IPAddress.Any, Port);
            IPEndPoint ip = new IPEndPoint(IPAddress.Any, 0);//linux any port
            byte[] bytes = udp.EndReceive(ar, ref ip);

            //string message = Encoding.ASCII.GetString(bytes);
            string message = string.Empty;
            foreach(var b in bytes)
            {
                message += (char)b;
            }

            //thread the message processing out as it shouldnt block our server
            (new System.Threading.Thread(() =>
            {
                ParseMessage(message, ip);
            })
            { IsBackground = true }).Start();
            

            StartListening();
        }

        private static void ParseMessage(string message, IPEndPoint ip)
        {
            if (message.StartsWith("/SERVER"))
                return;//self message ignore

            if(message.Length <=40)
            {
                var originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Message bad: " + message);
                Console.ForegroundColor = originalColor;

#if DEBUG
                originalColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Check Sketch (ESP8266wifi.h) for buffer size and set it to 128, instead of 16");
                Console.ForegroundColor = originalColor;
#endif

                return;
            }

            Console.WriteLine("MSG from " + ip.Address + "\t" + message);

            //wait for sender to finish
            System.Threading.Thread.Sleep(100);


            var deviceId = message.Substring(0, 36);//70D9A386-9D32-46A6-910D-E029EF3B0E9E
            message = message.Substring(36,message.Length-37);

            //format is iether R or W
            if (message.StartsWith("/R/") || message.StartsWith("/W/"))
            {
                ProcessMessage(message, deviceId, ip);
            }

            //update device stats
            var dmi = Devices.DeviceManager.Devices.Where(a => a.GUID == deviceId).First();
            dmi.IP = ip.Address;
            dmi.LastSeen = DateTime.Now;
        }

        private static void ProcessMessage(string message, string device, IPEndPoint ip)
        {
#if DEBUG
            var originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(message);
            Console.ForegroundColor = originalColor;
#endif
            var request = message.Split('/');
            //first is R
            //second is requested read
            //third is parameter

            //get our device
            var dev = Devices.DeviceManager.Devices.Where(a => a.GUID == device).First();

            if (dev == null)
                Devices.DeviceManager.Devices.Add(new Devices.Device()
                {
                    Created = DateTime.Now,
                    DeviceType = Devices.DeviceType.Unknown,
                    GUID = device,
                    IP = ip.Address,
                    LastSeen = DateTime.Now,
                });

            if(dev.DeviceType == Devices.DeviceType.RelayStation)
            {
                if (!Devices.RelayStation.readCommands.Contains(request[2]))
                {
                    Console.WriteLine("Unknown request " + request[1]);
                    return;
                }

                Devices.RelayStation rs = new Devices.RelayStation();//loads from disk everytime, not optimal

                switch (request[2])
                {
                    case "ADVERTISE":
                        rs.ProcessAdvertise(device, ip);
                        break;
                    case "VERSION":
                        Console.WriteLine("Version " + request[3]);
                        break;
                    case "PIN_STATE":
                        rs.UpdatePinState(device, request[3], request[4]);
                        break;
                    case "COMMANDS":
                        Console.WriteLine(request[3]);
                        break;
                    case "CONNECTED":
                        rs.ProcessConnected(device, ip, request[3]);
                        break;
                    default:
                        Console.WriteLine("Unknown request " + request[1]);
                        break;
                }

                //update xml file of current pin state
                rs.Save();
            }
            else
            {
                Console.Write("Unable to process this device type (" + dev.DeviceType.ToString() + ")");
            }
        }
        
        public static void SendCommand(IPEndPoint ip, string v)
        {
#if DEBUG
            var originalColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Sending: " + v);
            Console.ForegroundColor = originalColor;
#endif

            var data = Encoding.ASCII.GetBytes(v);
            Listener.udp.Send(data, data.Length, ip);
        }

        public static void SendCommand(string v)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Listener.Port);//broadcast message

            var data = Encoding.ASCII.GetBytes(v);
            Listener.udp.Send(data, data.Length, ip);
        }
    }
}
