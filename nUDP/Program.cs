﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace nUDP
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = System.AppDomain.CurrentDomain.FriendlyName;
            var version = Assembly.GetEntryAssembly().GetName().Version;

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(name + "\t" + version);
            Console.ForegroundColor = ConsoleColor.DarkGray;

            if(args.Contains("-h") || args.Contains("--help"))
            {
                PrintHelp();
                return;
            }

            Devices.RelayStation rs;

            #region Init New System
            if (args.Contains("-i"))
            {
                //export our hard coded schedule
                Devices.DeviceManager.Devices.Add(new Devices.Device()
                {
                    Created = DateTime.Now,
                    DeviceType = Devices.DeviceType.RelayStation,
                    GUID = "70D9A386-9D32-46A6-910D-E029EF3B0E9E",
                });
                Devices.DeviceManager.SaveToDisk();

                rs = new Devices.RelayStation();
                var hardPins = rs.HardCodedStationExample();
                rs.Export("70D9A386-9D32-46A6-910D-E029EF3B0E9E.xml", hardPins);
            }

            //todo add -a "ADD"
            #endregion

            rs = new Devices.RelayStation();

            Devices.DeviceManager.LoadFromDisk();

            Listener.StartListening();

            //m = Manual
            if (args.Contains("-m"))
            {
                Console.WriteLine("Press Q to exit...");
                ConsoleKeyInfo key = Console.ReadKey();

                while (key.Key != ConsoleKey.Q)
                {
                    key = Console.ReadKey();
                    Listener.SendCommand(key.ToString());
                }
            }
            else
            {
                Thread.Sleep(Timeout.Infinite);
            }

            Devices.DeviceManager.SaveToDisk();
        }

        private static void PrintHelp()
        {
            Console.WriteLine("-a /t Add new device");
            Console.WriteLine("-i /t Init test device");
            Console.WriteLine("-m /t Run in manual mode");
        }
    }
}
